
local BALLL1, BALLL2 = MT_STARWEAVER_CHASER_HORIZONTAL, MT_STARWEAVER_CHASER_VERTICAL

local function fireball(mo) --continuous thinker
    if (mo.z - mo.floorz) < 24*mo.scale then
        mo.z = mo.floorz + (24*mo.scale)
    end
    if mo.theball then
        mo.theball = $ + 1 --detect the oldest existing fireball and delete new ones if old one still exists
    end
end

--spawn thinker
local function ballsonfire(mo)
    mo.theball = 1
    for ball in mobjs.iterate() do
        if ball.type ~= BALLL1 and ball.type ~= BALLL2 then
            continue
        end
        if ball.theball and ball.theball > mo.theball then
            mo.fuse = 1
            mo.state = S_INVISIBLE
            return
        end
    end
end

addHook("MobjSpawn", ballsonfire, BALLL1)
addHook("MobjSpawn", ballsonfire, BALLL2)
addHook("MobjThinker", fireball, BALLL1)
addHook("MobjThinker", fireball, BALLL2)

local function livetogetherdietogether()
    for summons in mobjs.iterate() do
        if summons.type ~= BALLL1 and summons.type ~= BALLL2 and summons.type ~= MT_SW_REPLIKA_GUN and summons.type ~= MT_BUMBLEBORE and summons.type ~= MT_SW_REPLIKA_VILE and summons.type ~= MT_JETTBOMBER and summons.type ~= MT_JETTGUNNER and summons.type ~= MT_SW_REPLIKA_SHOCK then
            continue
        end
        --print("die")
        P_KillMobj(summons)
    end
    if modeattacking then
		for p in players.iterate do
			P_DoPlayerExit(p)
		end
    end
end

addHook("MobjDeath", livetogetherdietogether, MT_STARWEAVER)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_SANDY
end, MT_SW_REPLIKA_GUN)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_OCEAN
end, MT_SW_REPLIKA_SHOCK)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_KETCHUP
end, MT_SW_REPLIKA_VILE)