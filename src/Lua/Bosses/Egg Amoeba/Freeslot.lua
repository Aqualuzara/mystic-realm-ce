--Boss
freeslot(
	"MT_EGGAMOEBA",
	"S_EGGAMOEBA_START1",
	"S_EGGAMOEBA_START2",
	"S_EGGAMOEBA_START3",
	"S_EGGAMOEBA_START4",
	"S_EGGAMOEBA_JUMP1",
	"S_EGGAMOEBA_JUMP2",
	"S_EGGAMOEBA_JUMP3",
	"S_EGGAMOEBA_JUMP4",
	"S_EGGAMOEBA_JUMP5",
	"S_EGGAMOEBA_JUMP6",
	"S_EGGAMOEBA_SPOUT1",
	"S_EGGAMOEBA_SPOUT2",
	"S_EGGAMOEBA_SPOUT3",
	"S_EGGAMOEBA_SPOUT4",
	"S_EGGAMOEBA_SPOUT5",
	"S_EGGAMOEBA_SPOUT6",
	"S_EGGAMOEBA_POGO",
	"S_EGGAMOEBA_PINCHPREP1",
	"S_EGGAMOEBA_PINCHPREP2",
	"S_EGGAMOEBA_PINCH1",
	"S_EGGAMOEBA_PINCH2",
	"S_EGGAMOEBA_PINCH3",
	"S_EGGAMOEBA_PINCH4",
	"S_EGGAMOEBA_PINCH5",
	"S_EGGAMOEBA_PINCH6",
	"S_EGGAMOEBA_PINCH7",
	"S_EGGAMOEBA_PINCH8",
	"S_EGGAMOEBA_PINCH9",
	"S_EGGAMOEBA_PINCH10",
	"S_EGGAMOEBA_DIE1",
	"S_EGGAMOEBA_DIE2",
	"S_EGGAMOEBA_DIE3",
	"S_EGGAMOEBA_DIE4",
	"S_EGGAMOEBA_DIE5",
	"SPR_EGSL" --turns out the Egg Slimer sprites are badly offset, go figure
)
--Slime Shield
freeslot(
	"MT_SHIELDSLIME",
	"S_SHIELDSLIME",
	"S_SHIELDSLIME_POP",
	"SPR_SLIM"
)
