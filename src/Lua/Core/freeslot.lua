--freeslot stuff over here so we can reference it whenever needed instead of having random freeslots all over the place

--global stuff
freeslot(
    "MT_SUPERSPARKLES",
    "SPR_SUSK",
    "S_SUPERSPARK",
    "SKINCOLOR_SUPERSAPPHIRE1",
    "SKINCOLOR_SUPERSAPPHIRE2",
    "SKINCOLOR_SUPERBUBBLEGUM1",
    "SKINCOLOR_SUPERBUBBLEGUM2",
    "SKINCOLOR_SUPERMINT1",
    "SKINCOLOR_SUPERMINT2",
    "SKINCOLOR_SUPERRUBY1",
    "SKINCOLOR_SUPERRUBY2",
    "SKINCOLOR_SUPERWAVE1",
    "SKINCOLOR_SUPERWAVE2",
    "SKINCOLOR_SUPERCOPPER1",
    "SKINCOLOR_SUPERCOPPER2",
    "SKINCOLOR_SUPERAETHER1",
    "SKINCOLOR_SUPERAETHER2",
    "SKINCOLOR_MRCEHYPER1",
    "SKINCOLOR_MRCEHYPER2",
	"SKINCOLOR_MR_COMET",
	"SKINCOLOR_MR_VOID",
	"SKINCOLOR_BLANK",--hyper colors and objects

	"SPR_REBO",
	"MT_REBOUND",
	"S_REBOUND",
	"sfx_airdsh",
	"sfx_bounc1",
	"sfx_bounc2",
	"sfx_strdsh",
	"MT_REBOUNDFIREBALL_AURA",--rebound dash objects and sounds

    "MT_FAKETHOK",
    "MT_FAKESPIN",
    "S_FAKETHOK",
    "S_FAKESPIN",
    "SPR_FTHK",
    "SPR_FSPN",--glowy secret
    "SPR_MSTE",

    "S_FAKEEMERALD1",
    "S_FAKEEMERALD2",
    "S_FAKEEMERALD3",
    "S_FAKEEMERALD4",
    "S_FAKEEMERALD5",
    "S_FAKEEMERALD6",
    "S_FAKEEMERALD7",
    "S_FAKEEMERALD8"
)

freeslot(
    "MT_TLBRD",
    "MT_TLHME",
    "S_TAILSBIRD1",
    "S_TAILSBIRD2",
    "S_TAILSBIRD3",
    "S_TAILSBIRD4",
    "S_TAILSBIRDHOUSE",
    "SPR_FLTS"--tails super flickies
)

sfxinfo[freeslot("sfx_marioe")].caption = "Correct Solution"--agz infinite stairs secret

freeslot(
    "SPR_UNCEMB",
    "SPR_KEY0L",
    "SPR_KEY1S",
    "SPR_KEY2S",
    "SPR_KEY3S",
    "MT_FIRE_STONE",
    "MT_WATER_STONE",
    "MT_LIGHTNING_STONE"--elemental shards and the emblem hud
)

freeslot(
    "sfx_dltrun"--Deltarune item pickup sound
)

sfxinfo[sfx_dltrun].caption = "A secret is revealed!"

freeslot(
    "MT_GOGGLESCRAWLA_SLOW",
    "MT_GOGGLESCRAWLA_FAST",
    "MT_ULTRABUZZ",
    "MT_DASHERCRAWLA",
    "MT_COCONUTS",
	"MT_JCZCAKE",
	"S_CAKE3",
	"S_CAKE4"
)

-- TVZ Snaila
/*
Notes from Henry:
The MT_* name of the snaila object
and the S_* names of the snaila states are changed
to shorten them (and also those fot blue snailas).
*/
freeslot(
    "S_PURPLESNAILA_STND",
    "S_PURPLESNAILA_RUN1",
    "S_PURPLESNAILA_RUN2",
    "S_PURPLESNAILA_RUN3",
    "S_PURPLESNAILA_RUN4",
    "S_PURPLESNAILA_RUN5",
    "S_PURPLESNAILA_RUN6",
    "S_PURPLESNAILA_RUN7",
    "SPR_GPOS",
    "MT_SLOWGOOP",
    "MT_PURPLESNAILA"
)

freeslot(
	"MT_BLUESNAILA",
	"S_BLUESNAILA_STND",
	"S_BLUESNAILA_RUN1",
	"S_BLUESNAILA_RUN2",
	"S_BLUESNAILA_RUN3",
	"S_BLUESNAILA_RUN4",
	"S_BLUESNAILA_RUN5",
	"S_BLUESNAILA_RUN6",
	"S_BLUESNAILA_RUN7",
	"SPR_POSG"
)

freeslot(
    "MT_EGGBALLER",
	"MT_EGGBALLER_FIRE"
)

freeslot(
    "MT_MFZTREE",
    "MT_EGGFREEZER",
    "MT_CRYOCRAWLA"
)

freeslot(
    "S_BROKENCRAWLA_STND",
    "S_BROKENCRAWLA_ATK1",
    "S_BROKENCRAWLA_ATK2",
    "S_BROKENCRAWLA_ATK3",
    "S_BROKENCRAWLA_ATKE",
    "MT_BROKENCRAWLA",
    "SPR_BCRW",
    "MT_LASERDUDE",
    "S_LASERDUDE_LASER3",
    "MT_LASERTARGET",
    "MT_EBOMB",
    "MT_EGGEBOMBER"
)

--aerial garden
freeslot(
    "sfx_topaz",
    "MT_FBOSS",
    "MT_FBOSS2",
    "MT_BEETLE",
    "MT_OLDCRAWLA",
    "SPR_RCRW",
    "MT_STARWEAVER",
    "MT_STARWEAVER_CHASER_HORIZONTAL",
    "MT_STARWEAVER_CHASER_VERTICAL",
    "MT_SW_REPLIKA_GUN",
    "MT_SW_REPLIKA_VILE",
    "MT_SW_REPLIKA_SHOCK"
)

for i = 1, 4, 1 do
	freeslot("S_PORTALSCRAP"..i)
end

--prismatic angel
freeslot(
	"SPR_ANGL",
	"SPR_TFLR",
	"MT_ANGEL",
	"MT_ANGELSPAWNER",
	"S_ANGEL1",
	"S_ANGEL2",
	"S_ANGEL3",
	"S_ANGEL4",
	"A_AngelThink",
	"sfx_grchm",
	"sfx_fwarn",
	"sfx_iwarn",
	"S_ANGEL_INDICATOR"
)

--mystic realm / dimension warp
freeslot(
    "MT_SUPER_ORB",
    "S_SUPER_ORB",
    "SPR_SORB",
    "MT_URING",
    "MT_XBOSS",
    "MT_EGGANIMUS",
    "MT_EGGANIMUS_EX",
    "sfx_spdash"
)