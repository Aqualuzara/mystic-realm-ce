-- placed here so it doesn't use it's own file lol
sfxinfo[sfx_kc5c].caption = "Shrine Activated"

local funniCE = {
	-- version string changes by git's bash script
	versionstring = "v0.6-indev",
	version = 6,
	subversion = -15,

	-- This is merely boolean for git.
	autobuild = false,

	hyperunlocked = false,
	elemshards = 0,
	shrine_active = false,
	secondquest = false,
	dowarptime = false,
	agz_portalopener = 0,
	sanctum_tradetimer = 0
}

rawset(_G, "mrce", funniCE)

-- Feel free to add into this block whatever
if mrce.autobuild then
	addHook("HUD", function(v)
		v.drawString(0, 0, "MRCE v0."..(mrce.version).." git build - "..(mrce.versionstring), V_60TRANS|V_SMALLSCALEPATCH|V_SNAPTOLEFT|V_SNAPTOTOP)
	end, "game")
end

local todist2 = R_PointToDist2
local FUNIT = FRACUNIT
local FBITS = FRACBITS
local _32IMAX = INT32_MAX
local tostr = tostring
local printlocal = print
local tonum = tonumber

local p_iter = players.iterate

local strsub = string.sub
local strlen = string.len
local strbyte = string.byte
local strchar = string.char
local strfind = string.find

local tabinsr = table.insert

local absv = abs

--Credit to Motd for this
--Credit to Lach for the OG
-- alignment: -1 = right-aligned, 1 = left-aligned
rawset(_G, "DrawMotdString",
-- Displays a string with a custom font
---@param v drawer
---@param x integer
---@param y integer
---@param scale fixed_t
---@param text string
---@param font string
---@param flags integer
---@param alignment integer
---@param color colormap
---@param string TRNSLATE translation
function(v, x, y, scale, text, font, flags, alignment, color, translation)
	local right
	local colormap = v.getColormap(0, color, translation)
	local start
	local finish
	alignment = $ or 1
	color = $ or 0
	right = alignment < 0
	text = tostr(text)

	if right then
		start = strlen(text)
    	finish = 1
	else
		start = 1
		finish = strlen(text)
	end

	for i = start, finish, alignment do
		local letter = font .. strbyte(text, i, i)
		if v.patchExists(letter) then

			local patch = v.cachePatch(letter)

			if right then -- right aligned, change offset before drawing
				x = $ - patch.width*scale
			end

			v.drawScaled(x, y, scale, patch, flags, colormap)

			if not right then -- left aligned, change offset after drawing
				x = $ + patch.width*scale
			end
		end
	end
end)

rawset(_G, "IsPlayerAround",
-- Perhaps cheaper way to guess if players are around the object
---@param mobj mobj_t
---@param distance fixed_t Distance between object and player
---@param update number Update interval
---@param delay  number To smooth out lag spikes, this is just delay to smooth it out
function(mobj, distance, update, delay)
	local delayx = delay or 0
	if not ((leveltime + delayx) % absv(update)) then return end
	local recorded = _32IMAX
	for player in p_iter do
		if (player.mo and player.mo.valid) then
			local mo = player.mo
			local dist = absv(todist2(mobj.x, mobj.y, mo.x, mo.y))

			if dist <= distance and recorded > dist then
				recorded = dist
				mobj.target = mo
			end
		end
	end

	if recorded ~= _32IMAX then return true end
	mobj.target = nil
	return false
end)

rawset(_G, "IsPlayerAroundBool",
-- Perhaps cheaper way to guess if players are around the object
---@param mobj mobj_t
---@param distance fixed_t Distance between object and player
---@param update number Update interval
---@param delay  number Updating it at one point is not that much useful, 
function(mobj, distance, update, delay)
	local delayx = delay or 0	
	if ((leveltime + delayx) % absv(update)) then return mobj.last_check end
	for player in p_iter do
		if (player.mo and player.mo.valid) then
			local mo = player.mo
			local dist = absv(todist2(mobj.x, mobj.y, mo.x, mo.y))

			if dist <= distance then
				mobj.last_check = true
				return true
			end
		end
	end
	mobj.last_check = nil
	return false
end)

rawset(_G, "IntToExtMapNum",
-- Borrowed from MapVote.lua
--IntToExtMapNum(n)
--Returns the extended map number as a string
--Returns nil if n is an invalid map number
---@param n number
function(n)
	if n < 0 or n > 1035 then
		return nil
	end
	if n < 10 then
		return "MAP0" + n
	end
	if n < 100 then
		return "MAP" + n
	end
	local x = n-100
	local p = x/36
	local q = x - (36*p)
	local a = strchar(65 + p)
	local b
	if q < 10 then
		b = q
	else
		b = strchar(55 + q)
	end
	return "MAP" + a + b
end)

rawset(_G, "GetNumberList",
--From CobaltBW. Thank you my dude!
--This is used to turn a level header variable into a table
--via detecting a "," as a separator.
---@param str string
function(str)
	local t = {}
	while str do
		local sep = strfind(str,'%,')
		if sep ~= nil then
			local arg = strsub(str,0,sep-1)
			local tag = tonum(arg)
			if tag then
				tabinsr(t, tag)
			elseif tag ~= 0 then
				printlocal('Invalid argument '..arg)
				break
			end
			str = strsub($,sep+1)
		else
			local tag = tonum(str)
			if tag then
				tabinsr(t, tag)
			end
			break
		end
	end
	return t
end)

-- Misc variables used throughout multiple scripts
rawset(_G, "pi", 22*FUNIT/7) -- Used in multiple objects and bosses
rawset(_G, "dispstaticlogo", false) -- Credits

rawset(_G, "debugmenu", false) -- Used to hide titlescreen elements for debug menus

rawset(_G, "ctrl_inputs", {
	-- movement
    up = {}, down = {}, left = {}, right = {},
    -- main
    jmp = {}, spn = {}, cb1 = {}, cb2 = {}, cb3 = {},
    -- sys
    sys = {}, pause = {}, con = {}
})

-- fill out these on map load
addHook("MapLoad", function()
    ctrl_inputs.up[1], ctrl_inputs.up[2] = input.gameControlToKeyNum(GC_FORWARD)
	ctrl_inputs.down[1], ctrl_inputs.down[2] = input.gameControlToKeyNum(GC_BACKWARD)
	ctrl_inputs.left[1], ctrl_inputs.left[2] = input.gameControlToKeyNum(GC_STRAFELEFT)
	ctrl_inputs.right[1], ctrl_inputs.right[2] = input.gameControlToKeyNum(GC_STRAFERIGHT)

	ctrl_inputs.jmp[1], ctrl_inputs.jmp[2] = input.gameControlToKeyNum(GC_JUMP)
	ctrl_inputs.spn[1], ctrl_inputs.spn[2] = input.gameControlToKeyNum(GC_SPIN)
	ctrl_inputs.cb1[1], ctrl_inputs.cb1[2] = input.gameControlToKeyNum(GC_CUSTOM1)
    ctrl_inputs.cb2[1], ctrl_inputs.cb2[2] = input.gameControlToKeyNum(GC_CUSTOM2)
    ctrl_inputs.cb3[1], ctrl_inputs.cb3[2] = input.gameControlToKeyNum(GC_CUSTOM3)

	ctrl_inputs.sys[1], ctrl_inputs.sys[2] = input.gameControlToKeyNum(GC_SYSTEMMENU)
	ctrl_inputs.pause[1], ctrl_inputs.pause[2] = input.gameControlToKeyNum(GC_PAUSE)
    ctrl_inputs.con[1], ctrl_inputs.con[2] = input.gameControlToKeyNum(GC_CONSOLE)


	if (GlobalBanks_Array[0] & (1 << (15))) then
		if not (mrce.elemshards & (1 << (0))) then
			mrce.elemshards = $ + 1
		end
	else
		if (mrce.elemshards & (1 << (0))) and not multiplayer then
			mrce.elemshards = min($ - 1, 0)
		end
	end
	if (GlobalBanks_Array[0] & (1 << (16))) then
		if not (mrce.elemshards & (1 << (1))) then
			mrce.elemshards = $ + 2
		end
	else
		if (mrce.elemshards & (1 << (1))) and not multiplayer then
			mrce.elemshards = min($ - 2, 0)
		end
	end
	if (GlobalBanks_Array[0] & (1 << (17))) then
		if not (mrce.elemshards & (1 << (2))) then
			mrce.elemshards = $ + 4
		end
	else
		if (mrce.elemshards & (1 << (2))) and not multiplayer then
			mrce.elemshards = min($ - 4, 0)
		end
	end
end)

-- position/flag values for the hud elements
rawset(_G, "hudpos", {
	yb_time = {
		x = 152,
		y = 100,
	},
	yb_guard = {
		x = 152,
		y = 100,
	},
	yb_ring = {
		x = 152,
		y = 117,
	},
	yb_total = {
		x = 152,
		y = 151,
	},
	yb_perfect = {
		x = 152,
		y = 151,
	},
	yb_link = {
		x = 141,
		y = 108,
	},
	yb_score = {
		x = 141,
		y = 125,
	},
	yb_downscore = {
		x = 141,
		y = 142,
	},
	yb_continue = {
		x = 141,
		y = 151,
	},
})

--hijacking this for my own global  function stuff instead of it having its own file --Xian

addHook("PreThinkFrame", function()
    for p in players.iterate do
		if p.realmo and not p.spectator and p.mrce == nil then
			local pce = {
			glowaura = 0,
			flycheat = false,
			hypercheat = false,
			canhyper = false,
			ultrastar = false,
			hyperimages = false,
			hypermode = false,
			customskin = 0,
			dontwantphysics = false,
			physics = true,
			skipmystic = false,
			nasyamystic = false,
			exspark = false,
			ishyper = false,
			jump = 0,
			spin = 0,
			shield = 0,
			c1 = 0,
			exsparkcolor = R_GetColorByName("Galaxy"),
			camroll = 0,
			cosmichysteria = false,
			speen = 0,
			freezeeffect = 0,
			hud = 1,
			constext = 0,
			forcehyper = 0,
			oldz = 0,
			snowboard = false,
			spinkick = 0,
			realspeed = 0,
			secmus = 0,
			glide = 0,
			floatpause = 0,
			spawnshield = 0,
			hyperlast = false,
			elemdive = 0,
			dashcancel = 0,
			aprilfools = 0,
			}
			if p.mo then
				p.mrce = pce
			end
		end
	end
end)

if not (yakuzaBossTexts) then
	rawset(_G, "yakuzaBossTexts", {})
end

addHook("NetVars", function(net)
	mrce = net($)
end)
