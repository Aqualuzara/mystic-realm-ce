--[[

	|												|||
	|	MRCE Prompts - Backend						|||
	|												|||
	\-----------------------------------------------\||
	.\-----------------------------------------------\|
	
	|- @Contributors: Skydusk
	|- @2024/2025
	|
	|- Code's intended use-case was for Mystic Realms Community Edition. As long contributors are credited,
	|  You are free to use, replicate, edit or contribute to this framework outside of Mystic Realms.
]]

-- Function format: mrceprompts.add(definition)

--#region internals
local prompts = {}
--#endregion
--#region class definitions

---@class mrceprompt_t
---@field duration 	number		how long should prompt last
---@field easepoint number		when it should move in/out
---@field drawer 	function	drawer function format: function(drawer v, mrceprompt_t prompt)
---@field tic 		number		current tic (internal)
---@field key 		number		table index (internal)
---@field offset_y 	number		offset (internal)
---@field y 		number		y position (read-only, please)

---@class mrcedefprompt_t		definition version
---@field duration 	number		how long should prompt last
---@field easepoint number		when it should move in/out
---@field drawer 	function	drawer function format: function(drawer v, mrceprompt_t prompt)

--#endregion
--#region class
rawset(_G, "mrceprompts", {

	-- Adds prompt onto stack (Steam Achievement-styled prompt in lower right corner)
	---@param prompt mrcedefprompt_t
	add = function(prompt)

		---@cast prompt mrceprompt_t
		if prompt.duration and prompt.easepoint and prompt.drawer then
			-- important values
			prompt.duration = prompt.duration
			prompt.easepoint = prompt.easepoint
			prompt.drawer = prompt.drawer

			-- setup
			prompt.tic = 0
			prompt.key = #prompts+1
			prompt.offset_y = 0
			prompt.y = 0

			table.insert(prompts, prompt)
		end
	end,
})
--#endregion
--#region drawer hook
addHook("HUD", function(v)
	for key, prompt in ipairs(prompts) do
		if key > 6 then break end
		local max_ticrprompt = prompt.duration
		local easingnum = prompt.easepoint
		local awayprompt = prompt.duration - easingnum

		local current_tic = prompt.tic
		prompt.offset_y = ease.linear(min(current_tic, easingnum)*FRACUNIT/easingnum, 0, 16)
		+ease.linear(max(current_tic-awayprompt, 0)*FRACUNIT/easingnum, 16, 0)
		prompt.y = TBSlib.reachNumber(prompt.y, (key-1)*32+prompt.offset_y, 6)

		prompt.drawer(v, prompt)

		prompt.tic = $+1
		prompt.key = key
		if prompt.tic == max_ticrprompt then
			table.remove(prompts, key)
			table.sort(prompts, function(a, b)
				return a.key < b.key
			end)
		end
	end
end)
--#endregion