--[[

	|												|||
	|	MRCE Achievements --- Framework				|||
	|												|||
	\-----------------------------------------------\||
	.\-----------------------------------------------\|
	
	|- @Contributors: Skydusk
	|- @2024/2025
	|
	|- Code's intended use-case was for Mystic Realms Community Edition. As long contributors are credited,
	|  You are free to use, replicate, edit or contribute to this framework outside of Mystic Realms.
]]

--#region documentation function palette
--
-- CLASS MRCE_Achivements 
--
---------------
-- >PROPERTIES:
---------------
--
-- #MRCE_Achivements == number of definitions (or total amount of existing achievements)
--
-- >METHODS:
--
--	MRCE_Achivements.add(table: definition) -> integer: achievement index
-- 	| adds new achievement onto stack
--
--	MRCE_Achivements.isUnlocked(integer: achievement index) -> boolean
-- 	| checks if achievement is unlocked
--
--	MRCE_Achivements.progress(void) -> fixed_t progress (100% == FRACUNIT) + excess
-- 	| calculates progress of unlocked achievements
--
--	MRCE_Achivements.recount(void) -> number
-- 	| recounts number of unlocked achievements
--
--	MRCE_Achivements.searchByCategory(string: category) -> table : achievements
-- 	| provides table of achievements of one category
--
--	MRCE_Achivements.searchByID(string: id_string) -> achievement, integer: achievement index
-- 	| searches by string ID a achievement
--
--	MRCE_Achivements.unlock(string|number: achievement id, sfx: sound effect index, player: player_t)
-- 	| unlocks achievement
--
----------------------
-- >LINEDEF EXECUTORS:
----------------------
--
-- "MRCEACHI"
-- | Unlocks specific achievement
-- |
-- | Choose only 1 out of these 2 of identification
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: number index of achievement
-- |- line stringargs{] -- in level editor arg + 1
-- |--- arg1: string identification of achievement
--
--
-- "MRCECHKA"
-- | Checks if achievement is unlocked -> activates tag based on args[0]
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: execute tag
-- |- line stringargs{] -- in level editor arg + 1
-- |--- arg1: string identification of achievement
--
--
-- "MRCEACCO"
-- | Counts how many achievements are unlocked and if they reached limit -> activates tag based on args[0]
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: execute tag
-- |--- arg1: min achievements unlocked (optional)
-- |--- arg2: max achievements unlocked (optional)
--
--
--#endregion
--#region prompt drawer

-- Function to draw prompt, used in different part of the project
---@param v void?
---@param prompt table
local function prompt_achv(v, prompt)
	v.draw(320-128, 200-prompt.y, v.cachePatch("PROMPTGPH"), V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	local icon_color = v.getColormap(TC_DEFAULT, prompt.iconcolor or SKINCOLOR_BLUE)

	if prompt.icon then
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch(prompt.icon), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	else
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch("TABEMBICON"), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	end

	v.drawString(320-102, 203-prompt.y, '\x8B'..'Achievement Unlocked!', V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	if string.len(prompt.name) > 20 then
		local string_1 = string.sub(prompt.name, 1, 20)
		local string_2 = string.sub(prompt.name, 21, string.len(prompt.name))
		if string.len(string_2) > 20 then
			string_2 = string.sub(string_2, 1, 17).."..."
		end

		v.drawString(320-102, 211-prompt.y, string_1, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
		v.drawString(320-102, 219-prompt.y, string_2, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")

	else
		v.drawString(320-102, 211-prompt.y, prompt.name, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	end
end

--#endregion
--#region class definitions

---@class achdefinition_t
---@field id 			string?  				string identification
---@field set 			table|string?  			category
---@field name 			string?  				name of the achievement
---@field desc 			string?					long description default to "NULL" if not short description
---@field minidesc 		string?					short description default to "NULL"
---@field tips 			string?					tips to how to get the achievement default to "NULL"
---@field func 			function?				function that gets called when unlocked
---@field gamesprite 	number?					index of sprite
---@field hudsprite 	string?					name of the hud sprite
---@field frame 		number?					frame of the sprite
---@field color 		number?					color of the sprite
---@field typein 		string?					type of achievement
---@field hidden 		boolean?				gives it hidden status
---@field extras 		boolean?				gives it shadow achievement status
---@field requirements 	table?					blocks unlock if these achievements are not unlocked yet
---@field inlevel 		number|string?			forces achievement to be unlocked only in this level

---@class achievement_t
---@field id 			string  				string identification
---@field set 			table|string  			category
---@field name 			string  				name of the achievement
---@field desc 			string					long description default to "NULL" if not short description
---@field minidesc 		string					short description default to "NULL"
---@field tips 			string					tips to how to get the achievement default to "NULL"
---@field func 			function				function that gets called when unlocked
---@field gamesprite 	number					index of sprite
---@field hudsprite 	string					name of the hud sprite
---@field frame 		number					frame of the sprite
---@field color 		skincolor_t|number		color of the sprite
---@field typein 		string					type of achievement
---@field flags 		number					various flags
---@field extras 		boolean					gives it shadow achievement status
---@field requirements 	table					blocks unlock if these achievements are not unlocked yet
---@field inlevel 		number|string			forces achievement to be unlocked only in this level

---@class achivementindex : number

--#endregion
--#region internals

local MRCE_Achivements = {unlocked = 0} ---@generic MRCE_Achivements
local identificators = {}
local id_leveldatabase = {}
local id_categorydb = {global = {}}

local non_extra_total = 0

--#endregion
--#region methods

---@param info achdefinition_t
---@return? achivementindex -- index of added achievement
--* --> info table schema:
--* .id: 
--* --> datatype: string defaults to "NULL"
--* --> description: string identification
--*
--* .set: 
--* --> datatype: string/table
--* --> description: belong to what categories
--*
--* .name: 
--* --> datatype: string defaults to "NULL"
--*
--* .desc: 
--* --> datatype: string 
--* --> description: long description default to "NULL" if not short description
--*
--* .minidesc: 
--* --> datatype: string 
--* --> description: short description default to "NULL"
--*
--* .tips: 
--* --> datatype: string 
--* --> description: tips to how to get the achievement default to "NULL"
--*
--* .func: 
--* --> datatype: function 
--* --> description: function that gets called when unlocked
--*
--* .gamesprite: 
--* --> datatype: sprite index (number)
--* --> description: index of sprite
--*
--* .hudsprite: 
--* --> datatype: string
--* --> description: name of the hud sprite
--*
--* .frame: 
--* --> datatype: frame index (number)
--* --> description: frame of the sprite
--*
--* .color: 
--* --> datatype: skincolor index (number)
--* --> description: color of the sprite
--*
--* .typein: 
--* --> datatype: string
--* --> description: type of achievement
--*
--* .hidden: 
--* --> datatype: boolean
--* --> description: gives it hidden status
--*
--* .extras:
--* --> datatype: boolean
--* --> description: gives it shadow achievement status
--*
--* .requirements: 
--* --> datatype: table of indexes
--* --> description: blocks unlock if these achievements are not unlocked yet
--*
--* .inlevel: 
--* --> datatype: number/string
--* --> description: forces achievement to be unlocked in level
--*
function MRCE_Achivements.add(info)
	if type(info) ~= "table" then
		error('[Add Achivement] Not a table')
		return
	end

	-- setup temp
	local index = {} ---@type achievement_t

	-- Metadata

	if info.name then
		index.name = tostring(info.name)
	else
		index.name = "NULL"
	end

	if info.desc then
		index.desc = tostring(info.desc)
		if info.minidesc then
			index.minidesc = tostring(info.desc)
		end
	elseif info.minidesc then
		index.desc = tostring(info.minidesc)
		index.minidesc = tostring(info.desc)
	else
		index.desc = "NULL"
	end

	if info.tips then
		index.tips = tostring(info.tips)
	else
		index.tips = "NULL"
	end

	-- active upon unlocking

	if info.func then
		index.func = info.func
	end

	-- spriteinfo

	if info.gamesprite and type(info.gamesprite) == 'number' then
		index.gamesprite = info.gamesprite
	end

	if info.hudsprite and type(info.hudsprite) == 'string' then
		index.hudsprite = info.hudsprite
	end

	if info.frame then
		index.frame = info.frame
	else
		index.frame = A
	end

	if info.color then
		index.color = info.color
	else
		index.color = SKINCOLOR_BLUE
	end

	-- Properties

	index.flags = 0	 -- compression of flags if necessary
	if info.hidden then
		index.flags = $|1
	end

	if info.typein and type(info.typein) == "string" then
		index.typein = info.typein
	else
		index.typein = "Generic"
	end

	if info.requirements then
		if type(info.requirements) == "table" then
			index.requirements = info.requirements
		else
			error('[Add Achivement] Requirements - Not a table')
		end
	end

	if info.inlevel then
		local num = info.inlevel

		if type(info.inlevel) == "string" then
			num = G_FindMapByNameOrCode(info.inlevel)
		end

		if type(num) == "number" then
			if not id_leveldatabase[num] then
				id_leveldatabase[num] = {}
			end
			table.insert(id_leveldatabase[num], #MRCE_Achivements+1)

			index.inlevel = num
		end
	end

	if info.id then
		index.id = info.id
		identificators[info.id] = #MRCE_Achivements + 1
	else
		index.id = "NULL"
	end

	-- This is in order to be done first
	-- extra is shadow achivements that do not count to %
	if info.extras then
		index.extras = true
	else
		non_extra_total = $+1
		index.extras = false
	end

	-- Categorization
	if info.set then
		if type(info.set) == "string" then
			index.set = info.set
			if not id_categorydb[info.set] then
				id_categorydb[info.set] = {}
			end

			table.insert(id_categorydb[info.set], #MRCE_Achivements + 1)
		elseif type(info.set) == "table" then
			local sets = info.set
			for i = 1, #sets do
				local set = sets[i]
				if type(set) == "string" then
					index.set = info.set
					if not id_categorydb[set] then
						id_categorydb[set] = {}
					end

					table.insert(id_categorydb[set], #MRCE_Achivements + 1)
				end
			end
		else
			index.set = "Global"
			table.insert(id_categorydb.global, #MRCE_Achivements + 1)
		end
	else
		index.set = "Global"
		table.insert(id_categorydb.global, #MRCE_Achivements + 1)
	end


	table.insert(MRCE_Achivements, index)
	return #MRCE_Achivements
end

-- Iterative method of finding, not recommended
---@param name string
---@return achievement_t?, achivementindex?
---@deprecated
function MRCE_Achivements.searchByName(name)
	for id = 1, #MRCE_Achivements do
		local entry = MRCE_Achivements[id]
		if name == entry.name then
			return entry, id
		end
	end
end

-- Gets achievement by ID from LUT, preferable method.
---@param id string achievement_t->id
---@return achievement_t entry
---@return achivementindex id
function MRCE_Achivements.searchByID(id)
	if identificators[id] and MRCE_Achivements[identificators[id]] then
		return MRCE_Achivements[identificators[id]], identificators[id]
	end
end

-- Gets a list of achievements by category string from LUT
---@param category string achievement_t->category[i]
---@return table? result
function MRCE_Achivements.searchByCategory(category)
	if id_categorydb[category] then
		local array = id_categorydb[category]
		local result = {}

		for i = 1, #array do
			local ach = array[i] ---@type achievement_t
			table.insert(result, ach)
		end

		return result
	end
end

-- Recounts all available achievements
---@return number result
function MRCE_Achivements.recount()
	local save = MRCE_Unlock.getSaveData().achivements
	local result = 0

	for _,v in pairs(save) do
		if v then
			result = $+1
		end
	end

	MRCE_Achivements.unlocked = result
	return result
end

-- Checks if achievement is unlocked
---@param id achivementindex index not string id
---@return boolean
function MRCE_Achivements.isUnlocked(id)
	local save = MRCE_Unlock.getSaveData().achivements
	if save[id] then
		return true
	else
		return false
	end
end

-- Calculates unlock progress
--* 1.00 (FRACUNIT) == 100%
---@return fixed_t
function MRCE_Achivements.progress()
	return MRCE_Achivements.unlocked * FRACUNIT / non_extra_total
end

-- Unlocks achievement
---@param query_id 	achivementindex|string 		can be index or string id
---@param sfx 		number? 					optional for sounds
---@param player 	player_t?					optional for sounds
function MRCE_Achivements.unlock(query_id, sfx, player)
	local info_id = query_id
	if type(info_id) == "string" then
		local entry
		entry, info_id = MRCE_Achivements.searchByID(info_id)
	end

	if type(info_id) == "number" and MRCE_Achivements[info_id] then
		local achivement = MRCE_Achivements[info_id]
		local save = MRCE_Unlock.getSaveData().achivements

		if achivement.requirements then
			for _,v in ipairs(achivement.requirements) do
				if not save[v] then
					return
				end
			end
		end

		if player then
			S_StartSound(nil, sfx or sfx_chchng, player)
		end

		if achivement.inlevel and gamemap ~= achivement.inlevel then
			return
		end

		if not save[info_id] then
			save[info_id] = true -- save existence

			MRCE_Achivements.recount()
			
			if achivement.func then
				achivement.func()
			end

			mrceprompts.add{
				duration = 8*TICRATE,
				easepoint = TICRATE/2,
				drawer = prompt_achv,
				name = achivement.name,
				icon = achivement.hudsprite,
				iconcolor = achivement.color
			}
		end
	end
end
--#endregion
--#region medal objects

-- Achivement Granting General Object
local mobjinfo_id = freeslot("MT_MRCEACHIVEMENT")

mobjinfo[mobjinfo_id] = {
--$Category MRCE Generic
--$Name Achivement Medal
--$Sprite MECEA0
--$Arg0 Order
--$Arg1 Frame
--$NotAngled
	doomednum = 831,
	spawnstate = S_EMBLEM1,
	spawnhealth = 1000,
	reactiontime = 8,
	deathstate = S_SPRK1,
	deathsound = sfx_ncitem,
	speed = 1,
	radius = 16*FRACUNIT,
	height = 30*FRACUNIT,
	mass = 4,
	flags = MF_SPECIAL|MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

---@param medal mobj_t
---@param mt mapthing_t
addHook("MapThingSpawn", function(medal, mt)
	if id_leveldatabase[gamemap] and id_leveldatabase[gamemap][mt.args[0]] then
		medal.extravalue1 = id_leveldatabase[gamemap][mt.args[0]]
		local save = MRCE_Unlock.getSaveData().achivements
		if save[medal.extravalue1] then
			medal.extravalue2 = 1
		end
	end
	if mt.stringargs[1] then
		local entry
		entry, medal.extravalue1 = MRCE_Achivements.searchByID(mt.stringargs[1])
		local save = MRCE_Unlock.getSaveData().achivements
		if save[medal.extravalue1] then
			medal.extravalue2 = 1
		end

		if entry.gamesprite then
			medal.sprite = entry.gamesprite
		end

		if entry.frame then
			medal.frame = entry.frame
		end

		if entry.color then
			medal.color = entry.color
		end
	end

	if mt.stringargs[0] then
		if _G[mt.stringargs[0]] and skincolors[_G[mt.stringargs[0]]] then
			medal.color = skincolors[_G[mt.stringargs[0]]]
		end
	end

	if medal.extravalue2 then
		medal.frame = $|FF_TRANS50
	end
end, mobjinfo_id)

---@param medal mobj_t
addHook("MobjSpawn", function(medal)
	medal.origz = medal.z
end, mobjinfo_id)

---@param medal mobj_t
addHook("MobjThinker", function(medal)
	if not (medal and medal.valid) then return end
	
	if not (medal.frame & FF_PAPERSPRITE) then
		medal.frame = $|FF_PAPERSPRITE
	end
	medal.angle = $ + FixedAngle(FRACUNIT)
	medal.z = medal.origz + 8 * abs(sin(FixedAngle(leveltime*4*FRACUNIT)))
	
	local sparkle = P_SpawnMobjFromMobj(medal, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK)
	sparkle.momx = P_RandomRange(-2, 2) * FRACUNIT
	sparkle.momy = P_RandomRange(-2, 2) * FRACUNIT
	sparkle.momz =  P_RandomRange(-2, 2) * FRACUNIT
	sparkle.colorized = true
	sparkle.color = medal.color
	sparkle.fuse = 10
	sparkle.scale = medal.scale *1/6
	sparkle.source = medal
	
	if (medal.frame & FF_TRANSMASK) then
		sparkle.flags2 = MF2_DONTDRAW
	end
end, mobjinfo_id)

--	Touch Hook
---@param medal mobj_t
addHook("TouchSpecial", function(medal)
	if medal.extravalue2 then
		return true
	end

	if medal.extravalue1 and not medal.extravalue2 then
		MRCE_Achivements.unlock(medal.extravalue1)
		medal.extravalue2 = 1
	end
end, mobjinfo_id)


--#endregion
--#region linedef executors

-- Unlocks specific achievement
--- choose only 1 out of these 2 of identification
--- line args{] -- in level editor arg + 1
--- arg0: number index of achievement
--- line stringargs{] -- in level editor arg + 1
--- arg1: string identification of achievement
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	if line.args[0] then
		MRCE_Achivements.unlock(line.args[0])
	end

	if line.stringargs[1] then
		if mo.player then
			MRCE_Achivements.unlock(line.stringargs[1], nil, mo.player)
		else
			MRCE_Achivements.unlock(line.stringargs[1])
		end
	end
end, "MRCEACHI")

-- Checks if achievement is unlocked -> activates tag based on args[0]
--- line args{] -- in level editor arg + 1
--- arg0: execute tag
--- line stringargs{] -- in level editor arg + 1
--- arg1: string identification of achievement
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	if line.stringargs[1] and line.args[0] 
	and line.frontsector
	and MRCE_Achivements.isUnlocked(line.stringargs[1]) then
		P_LinedefExecute(line.args[0], mo, line.frontsector)
	end
end, "MRCECHKA")

-- Counts how many achievements are unlocked and if they reached limit -> activates tag based on args[0]
--- line args{] -- in level editor arg + 1
--- arg0: execute tag
--- arg1: min achievements unlocked (optional)
--- arg2: max achievements unlocked (optional)
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	local count = MRCE_Achivements.recount()

	if line.stringargs[1] then
		if line.args{1} and line.args{1} > count then
			return
		end

		if line.args{2} and line.args{2} < count then
			return
		end

		if line.args[0] and line.frontsector then
			P_LinedefExecute(line.args[0], mo, line.frontsector)
		end
	end
end, "MRCEACCO")

--#endregion

--#globalsetup
MRCE_Achivements.recount()

rawset(_G, "MRCE_Achivements", MRCE_Achivements)