freeslot('SPR_MECE')
--[[
In-Level emblems go in this order: Frame A (waterdrop) + color Blue, Frame F (Lightning) + color Topaz,
Frame C (Leaf) + color Emerald, Frame D (Flame) + color Flame, and frame E (Snow) + color Icy
Special frames go to other purposes, Frame I (Prism) + color Black is used for emerald shrines
Time Emblems Frame T + color Bone, Ring Emblems Frame R + color Goldenrod
the first 9 frames actually represent each of the zones in order,
from Jade Coast (waterdrop) to the Mystic Realm (prism) but we won't use them quite like that
in future, separate stylized sprites of each of these sprites, based on each zone,
will instead be made, akin to the current Jade Coast Waterdrop Medal sprite

DEV TIMES
Dev tmes will be used as a bonus super challenge for each stage, with a significantly tougher time limit
and potentially skin-locked, but will be counted separately from normal acheivements, contributing
an additional 1% beyond 100% should the player manage to get them all
They will not be visible in the achievements list until after the standard Time emblem is earned
However, the functionality for this isn't ready yet, so I'll doc some of the expected times here for now

JCZ1 - 0:18
JCZ2 - 0:20
JCZ3 - 0:15
MHK  - 0:35
MFZ1 - 1:15
MFZ3 - 0:15

SHADOW ACHIEVEMENTS
optional achievements that are not counted normally like standard achievements,
these do not appear in the achievements list until they have been unlocked. They do not count towards 100%,
but unlocking all of them grants an additional 1% past 100%. They are ridiculously difficult and not remotely fair,
by no means would I expect anyone to try to go for them. Things like
clearing ultimate mode all emeralds + special stages on lockon as amy in less than 2 hours without using any continues or shields or whatever
but naturally, there will always be people that are crazy enough to try insane stuff like that,
and who am I to not at least acknowledge that?
Functionality is partially there, acheivements can be marked as EXTRA, so that they won't count towards the 100% tracker,
but I don't believe they are hidden from the achievement list when locked, nor do they count for the extra 1% yet

counting both Shadow Achievements and Dev Times,
the max % obtainable in the % tracker should be 102% as of now, once that functionality is complete
]]
--
-- GLOBAL
--

local playerdamagedlevel = false
local playerdamagedglobal = false

addHook("MobjDamage", function(mo)
	--if mapheaderinfo[gamemap].bonustype < 1 then return end
	if not playerdamagedlevel then playerdamagedlevel = true end
	if not playerdamagedglobal then playerdamagedglobal = true end
end, MT_PLAYER)

addHook("MapLoad", function(map)
	if playerdamagedlevel then playerdamagedlevel = false end
	if gamestate == GS_TITLESCREEN then
		playerdamagedglobal = false
	end
end)

local Masochist = MRCE_Achivements.add{		--the typo here really bothers me, why is it mispelled like that
	id = "Masochist",
	typein = "Task",
	name = "...",
	desc = "Are you okay?",
	tips = "Destroy 100 Eggman Monitors",
}

addHook("MobjDeath", function()
	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.eggboxes then
		stats.eggboxes = 1
	else
		stats.eggboxes = $+1
	end

	if stats.eggboxes > 100 then
		MRCE_Achivements.unlock(Masochist)
	end
end, MT_EGGMAN_BOX)

--
-- DUMMIES
--

--for i = 1, 50 do
--	MRCE_Achivements.add{}
--end -- Dummies

--
-- JCZ1
--

local JCZ1_Rings = MRCE_Achivements.add{
	id = "JCZ1_Rings",
	typein = "Ring Attack",
	set = {"JCZ", "JCZ1", "Rings"},
	name = "JCZ1 Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

local JCZ1_Time = MRCE_Achivements.add{
	id = "JCZ1_Time",
	typein = "Time Attack",
	set = {"JCZ", "JCZ1", "Time"},
	name = "JCZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 30 seconds",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 101 then return end
	if not p.exiting then return end
	if p.rings >= 300 and not MRCE_Achivements.isUnlocked(JCZ1_Rings) then
		MRCE_Achivements.unlock(JCZ1_Rings)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.realtime < 30*TICRATE and not MRCE_Achivements.isUnlocked(JCZ1_Time) then
		MRCE_Achivements.unlock(JCZ1_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

MRCE_Achivements.add{
	id = "JCZ1_Beach", -- unique identificator
	typein = "Medal",  -- How to get, this is not category
	set = {"JCZ", "JCZ1", "Medal"}, -- categories
	name = "First Medal!",
	desc = "Quite easy, ey?",
	tips = "In middle of starting open section...",
	inlevel = "A1", -- Merely helps with search in code and makes sure
	-- Also makes sure that unlock is only possible in said level.

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_House",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Beach House!",
	desc = "Definitely no creepy empty vibes.",
	tips = "A common trope, a secret behind the waterfall",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_Ocean",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Monolith Over Ocean.",
	desc = "View on droplets of world",
	tips = "Out of the cave, there is worth flying somewhere...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_RBDCave",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Goodies up the Cave Wall",
	desc = "Found a medal in Sonic's rebound cave",
	tips = "Learning to bounce up walls, but don't forget to look up",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_Lake",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Did Someone Drop This?",
	desc = "Found a medal sitting out in the lake",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "JCZ101",
}

local JCZ1_RedCrawlas = MRCE_Achivements.add{
	id = "JCZ1_RedCrawlas",
	typein = "Task",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Revenge of BLU.",
	desc = "It took 10 times of 3 and it still isn't out.",
	tips = "Destroy 30 times red crawlas in any JCZ1 playthrough...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_PEPPER,

	hudsprite = "JCZ101",
}

addHook("MobjDeath", function(mo)
	if not (mapheaderinfo[gamemap].lvlttl == "Jade Coast" and mapheaderinfo[gamemap].actnum == 1) then return end

	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.redcrawlas then
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', 1)
	else
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', stats.redcrawlas+1)
	end

	if stats.redcrawlas > 30 then
		MRCE_Achivements.unlock(JCZ1_RedCrawlas)
	end
end, MT_REDCRAWLA)

MRCE_Achivements.add{
	id = "JCZ1_Posters",
	typein = "Task",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Someone is missing?",
	desc = "Or someone is evading paying rent.",
	tips = "Collect all posters",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_ISLAND,

	hudsprite = "JCZ101",
}

--
-- JCZ2
--

local JCZ2_Rings = MRCE_Achivements.add{
	id = "JCZ2_Rings",
	typein = "Ring Attack",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "JCZ2 Ring Attack",
	desc = "",
	tips = "Collect 400 rings and reach the exit",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

local JCZ2_Time = MRCE_Achivements.add{
	id = "JCZ2_Time",
	typein = "Time Attack",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "JCZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 45 seconds",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 102 then return end
	if not p.exiting then return end
	if p.rings >= 400 and not MRCE_Achivements.isUnlocked(JCZ2_Rings) then
		MRCE_Achivements.unlock(JCZ2_Rings)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.realtime < 45*TICRATE and not MRCE_Achivements.isUnlocked(JCZ2_Time) then
		MRCE_Achivements.unlock(JCZ2_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

MRCE_Achivements.add{
	id = "JCZ2_CavePond",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Hidden Pond on the Cliff",
	desc = "A secret room near the final checkpoint",
	tips = "Instead of progressing up the cliffside, maybe try looking up",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ2_Waterfall",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Secret at the Foot of the Falls",
	desc = "A secret room near the bottom of the lone waterfall",
	tips = "A giant waterfall from above. Follow it down into a small cave.",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ2_HouseCliff",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Who's House is this?",
	desc = "A not so hidden path way up high",
	tips = "I climbed so far up, I can see my house from here!\n...Well, it's someone's house.",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ2_WaterRun",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "High Within the Coral Cave",
	desc = "Run across the water's surface to reach a hidden aclove",
	tips = "Hidden inside a watery cave. Perhaps with enough speed, one could stay dry?",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ2_Shrine",
	typein = "Task",
	set = {"JCZ", "JCZ2", "Task"},
	name = "Emerald Shrine by the Shore",
	desc = "High above the water, near the final climb, the blue emerald shrine awaits",
	tips = "All roads must eventually converge somewhere\nThe shrine looks down from above",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "JCZ101",
}

--
-- JCZ3
--
local JCZ3_Guard = MRCE_Achivements.add{
	id = "JCZ3_Guard",
	typein = "Damageless",
	set = {"JCZ", "JCZ3", "Medal"},
	name = "JCZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "A3",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "JCZ101",
}

local JCZ3_Time = MRCE_Achivements.add{
	id = "JCZ3_Time",
	typein = "Time Attack",
	set = {"JCZ", "JCZ3", "Medal"},
	name = "JCZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 30 seconds",
	inlevel = "A3",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 103 then return end
	if p.exiting and not playerdamagedlevel and not MRCE_Achivements.isUnlocked(JCZ3_Guard) then
		MRCE_Achivements.unlock(JCZ3_Guard)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.exiting and p.realtime <= 30*TICRATE and not MRCE_Achivements.isUnlocked(JCZ3_Time) then
		MRCE_Achivements.unlock(JCZ3_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

--
-- JCZ-Emerald
--

MRCE_Achivements.add{
	id = "MHK_Bridge",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Under the Bridge",
	desc = "unda",
	tips = "When you cross a bridge, jump to a platform below. There lies your emblem.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "MHK_Ledge",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "The High Ledge",
	desc = "High above in a small corridor",
	tips = "Three ring monitors, on a rock in a cavern. You'll need to climb up.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "MHK_Spikes",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Breaking Through",
	desc = "A room hiding behind some spikes",
	tips = "Flying off a ramp. Before the starpost, look back. Jump and break some spikes.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "MHK_Egg",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Turning Pain into Profit",
	desc = "With invincibility, bounce off an egghead from way up high",
	tips = "Bounce from a plateau and go flying through the sky to reach the platform.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

local MHK_Cake = MRCE_Achivements.add{
	id = "MHK_Cake",
	typein = "Task",
	set = {"MKZ", "Task"},
	name = "The Cake is a Lie",
	desc = "Found a cake, and pushed it over.",
	tips = "When life gives you a big old ramp to slide off of, get mad! Rebound back!",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "JCZ101",
}

addHook("MobjThinker", function(mo)
	--print("still alive")
	if mo.state == S_CAKE3 then
		MRCE_Achivements.unlock(MHK_Cake)
		--print("You're the moron they made to make me an idiot")
	end
end, MT_JCZCAKE)

local MHK_Rings = MRCE_Achivements.add{
	id = "MHK_Rings",
	typein = "Ring Attack",
	set = {"MHK", "Rings"},
	name = "MHK Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

local MHK_Time = MRCE_Achivements.add{
	id = "MHK_Time",
	typein = "Time Attack",
	set = {"MHK", "Time"},
	name = "MHK Time Attack",
	desc = "",
	tips = "Reach the exit in under 45 seconds",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

local MHK_SRank = MRCE_Achivements.add{
	id = "MHK_SRank",
	typein = "Task",
	set = {"MHK", "Task"},
	name = "MHK Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MRCEHYPER2,

	hudsprite = "JCZ101",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 123 then return end
	if not p.exiting then return end
	--print(mrce_emmystagerank)
	if p.rings >= 300 and not MRCE_Achivements.isUnlocked(MHK_Rings) then
		MRCE_Achivements.unlock(MHK_Rings)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.realtime < 45*TICRATE and not MRCE_Achivements.isUnlocked(MHK_Time) then
		MRCE_Achivements.unlock(MHK_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
	if mrce_emmystagerank >= 6 and not MRCE_Achivements.isUnlocked(MHK_SRank) then
		MRCE_Achivements.unlock(MHK_SRank)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

--
-- TVZ1
--

--
-- TVZ2
--

--
-- TVZ3
--

--
-- TVZ-Emerald
--

--
-- VFZ1
--

MRCE_Achivements.add{
	id = "VFZ1_TreeTop",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "Top of the forest",
	desc = "Climbed a hollow tree to it's highest branch",
	tips = "Inside a big tree. Normally you would go down. But instead, go up.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "VFZ1_WaterTree",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "The Apple that fell not far into the Water",
	desc = "Found a Medal hiding behind a tree in a large lake",
	tips = "Behind a small tree, in the deep part of a lake. Near a whirlwind shield.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "VFZ1_Lario",
	typein = "Task",
	set = {"VFZ", "VFZ1", "Task"},
	name = "Everybody wanna be a Superstar",
	desc = "Get a lot of money, drive a fancy car",
	tips = "Near the greatest slope, reject the promise of speed. Break open the wall",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "VFZ1_AltExit",
	typein = "Task",
	set = {"VFZ", "VFZ1", "Task"},
	name = "Another Way Out",
	desc = "Found the alternate exit",
	tips = "Try taking the high path",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "VFZ1_WaterFall",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "Over the Waterfall",
	desc = "Found a Medal hiding at the mouth of the falls",
	tips = "Above the triplets, Nearest the end of the wood. From here springs great life.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "TABEMBICON",
}

--
-- VFZ2
--

MRCE_Achivements.add{
	id = "VFZ2_Shrine",
	typein = "Task",
	set = {"VFZ", "VFZ2", "Task"},
	name = "Emerald Shrine of the Forest",
	desc = "Low below the tree houses, within the lower village, a shrine hide ",
	tips = "After jumping down, Amongst roots and glowing ponds. The village Tree Shrine.",
	inlevel = "A8",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

--
-- VFZ3
--

--
-- VFZ-Emerald
--

MRCE_Achivements.add{
	id = "LBW_Start",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Labyrinth Woods",
	desc = "Brave the forest labyrinth",
	tips = "Thanks for your patience. This next emblem location has been discarded",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "LBW_Reverse",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Go Back",
	desc = "Go back the way you came, and find something not previously there",
	tips = "L-E-A-V-E. Deeper past the winding floors; When you light that torch!",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "LBW_FakeWall",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Bush Behind the Wall",
	desc = "Crawlama",
	tips = "Space rends, all two paths bend to one definite end. This place don't stop there...",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "LBW_Fly",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Swinging on High",
	desc = "High above the bridges and swings",
	tips = "Go, go in! First room, higher off the flyover! Don't get spiked ringless.",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

local LBW_Rings = MRCE_Achivements.add{
	id = "LBW_Rings",
	typein = "Ring Attack",
	set = {"LBW", "Rings"},
	name = "LBW Ring Attack",
	desc = "",
	tips = "Collect 500 rings and reach the exit",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

local LBW_Time = MRCE_Achivements.add{
	id = "LBW_Time",
	typein = "Time Attack",
	set = {"LBW", "Time"},
	name = "LBW Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

local LBW_SRank = MRCE_Achivements.add{
	id = "LBW_SRank",
	typein = "Task",
	set = {"LBW", "Task"},
	name = "LBW Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MRCEHYPER2,

	hudsprite = "TABEMBICON",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 123 then return end
	if not p.exiting then return end
	--print(mrce_emmystagerank)
	if p.rings >= 500 and not MRCE_Achivements.isUnlocked(LBW_Rings) then
		MRCE_Achivements.unlock(LBW_Rings)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.realtime < 105*TICRATE and not MRCE_Achivements.isUnlocked(LBW_Time) then
		MRCE_Achivements.unlock(LBW_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
	if mrce_emmystagerank >= 6 and not MRCE_Achivements.isUnlocked(LBW_SRank) then
		MRCE_Achivements.unlock(LBW_SRank)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

--
-- FRZ1
--

--
-- FRZ2
--

--
-- FRZ3
--

--
-- FRZ-Emerald
--

--
-- MFZ1
--

--
-- MFZ2
--

--
-- MFZ3
--
local MFZ3_Guard = MRCE_Achivements.add{
	id = "MFZ3_Guard",
	typein = "Damageless",
	set = {"MFZ", "MFZ3", "Medal"},
	name = "MFZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AF",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

local MFZ3_Time = MRCE_Achivements.add{
	id = "MFZ3_Time",
	typein = "Time Attack",
	set = {"MFZ", "MFZ3", "Medal"},
	name = "MFZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 30 seconds",
	inlevel = "AF",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

local MFZ_Toasty = MRCE_Achivements.add{
	id = "MFZ_Toasty",
	typein = "Task",
	set = {"MFZ", "Task"},
	name = "Stayin' Toasty",
	desc = "",
	tips = "In a sigle run-through, clear all of Midnight Freeze Zone without dying or being frozen",
	inlevel = "AF",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

addHook("PlayerThink", function(p)
	if p.spectator then return end
	if not p.realmo then return end
	if p.mfrozenc and (p.frozen or p.frozentimer) then
		p.mfrozenc = false
	end
	if gamemap ~= 115 then return end
	if p.exiting and not playerdamagedlevel and not MRCE_Achivements.isUnlocked(MFZ3_Guard) then
		MRCE_Achivements.unlock(MFZ3_Guard)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.exiting and p.realtime <= 30*TICRATE and not MRCE_Achivements.isUnlocked(MFZ3_Time) then
		MRCE_Achivements.unlock(MFZ3_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.exiting and p.mfrozenc then
		MRCE_Achivements.unlock(MFZ_Toasty)
		S_StartSound(nil, sfx_chchng, p)
		p.mfrozenc = nil
	end
end)

addHook("PlayerSpawn", function(p)
	if mapheaderinfo[gamemap].lvlttl ~= "Midnight Freeze" and mapheaderinfo[gamemap].lvlttl ~= "Silver Cavern" then
		p.mfrozenc = nil
	else
		p.mfrozenc = $ or false
	end
	if gamemap == 113 and p.mo.y > 11328*FRACUNIT then --we spawned at the very start of mfz1
		p.mfrozenc = true
	end
end)

addHook("MobjDeath", function(mo)
	mo.player.mfrozenc = false
end, MT_PLAYER)

--
-- MFZ-Emerald
--

--
-- SPZ1
--

--
-- SPZ2
--

--
-- SPZ3
--
local SPZ3_Guard = MRCE_Achivements.add{
	id = "SPZ3_Guard",
	typein = "Damageless",
	set = {"SPZ", "SPZ3", "Medal"},
	name = "SPZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AI",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

local SPZ3_Time = MRCE_Achivements.add{
	id = "SPZ3_Time",
	typein = "Time Attack",
	set = {"SPZ", "SPZ3", "Medal"},
	name = "SPZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 30 seconds",
	inlevel = "AI",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

addHook("PlayerThink", function(p)
	if gamemap ~= 118 then return end
	if p.exiting and not playerdamagedlevel and not MRCE_Achivements.isUnlocked(SPZ3_Guard) then
		MRCE_Achivements.unlock(SPZ3_Guard)
		S_StartSound(nil, sfx_chchng, p)
	end
	if p.exiting and p.realtime <= 30*TICRATE and not MRCE_Achivements.isUnlocked(SPZ3_Time) then
		MRCE_Achivements.unlock(SPZ3_Time)
		S_StartSound(nil, sfx_chchng, p)
	end
end)

--
-- SPZ-Emerald
--

--
-- AGZ1
--

--
-- AGZ2
--

MRCE_Achivements.add{
	id = "AGZ2_Chandelier",
	typein = "Medal",
	set = {"AGZ", "AGZ2", "Medal"},
	name = "Over the Waterfall",
	desc = "Found a Medal on the chandelier of the gathering room",
	tips = "A chandelier hangs from the ceiling of a room where two paths converge.",
	inlevel = "AK",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "AGZ2_Bars",
	typein = "Medal",
	set = {"AGZ", "AGZ2", "Medal"},
	name = "Another way in",
	desc = "Found a way around the bars blocking entry",
	tips = "The second left path's top entrance is blocked with bars. Try going around.",
	inlevel = "AK",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "AGZ2_Vault",
	typein = "Medal",
	set = {"AGZ", "AGZ2", "Medal"},
	name = "Breaking the Bank",
	desc = "Found all the Line buttons and opened the Vault",
	tips = "Beginning fountain, second left and right paths, and the final temple.",
	inlevel = "AK",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achivements.add{
	id = "AGZ2_Shrine",
	typein = "Task",
	set = {"AGZ", "AGZ2", "Task"},
	name = "Emerald Shrine of the Sky",
	desc = "Found the 2 Triangle buttons and opened the way to the 7th Emerald Shrine",
	tips = "Triangle buttons on both the left and right paths will open a door.",
	inlevel = "AK",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

--
-- AGZ3
--

--
-- AGZ-Emerald
--

--
-- ISZ
--

--
-- PAZ1
--

--
-- PAZ2
--

--
-- PAZ3
--

--
-- MRZ
--

--
-- DWZ
--

--
-- BONUS
--

-- Leave this at very end so we could measure how many achivements we have.
print('[Mystic Realms CE] '..#MRCE_Achivements.." achivements added!")

addHook("NetVars", function(net)
	playerdamaged = net($)  --net sync that shit
	playerdamagedglobal = net($)
end)