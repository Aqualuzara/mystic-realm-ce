--credits
--CosmicSRB2: much of the foundations for this script because I'm clueless
--skeletondome & luigibudd: debugging
--Skydusk: teaching me this accursed language

--Script for Metal Sonic's Boost Drive ability

--Check by skeletondome: adds a routine to check if a button is held
addHook("PlayerThink", function(player)
    if (player.mo.skin ~= "metalsonic") then return end

    if not player.holdingjump and player.cmd.buttons & BT_JUMP ~= 0 then
        player.holdingjump = true
    elseif player.cmd.buttons & BT_JUMP == 0 then
        player.holdingjump = false
    end

	--msconv variable only allows this to happen once per ability: if jump is released before landing, vertical momentum is halved
	if (player.msconv == 1) then
		if (player.holdingjump == false) then
			player.msconv = 0
			--dont halve vertical momentum if going down
			if (player.mo.momz > 0) then
				player.mo.momz = $/2
			end
		end
	end
	--give metal their tank powers back on hitting the floor and reinitialise the msconv variable for momentum halving to be possible next ability activation
	if player.mo.eflags & MFE_JUSTHITFLOOR then
		player.msconv = -1
		player.charflags = $|SF_DASHMODE
	end
	if player.eeghosts == 1 then
		player.mtlghosttimer = $ - 1
	end
	if player.mtlghosttimer == 0 then
		P_SpawnGhostMobj(player.mo)
		player.mtlghostcount = $ - 1
	end
	if player.mtlghostcount == 0 then
		player.eeghosts = 0
	end
end)
--the meat of it all
addHook("AbilitySpecial", function(player)
	--setup ability, make sure youre only hitting metal succ
		if player.mo.skin == "metalsonic" then
		   --overwrite base ability
		   player.charability = CA_NONE
		end
		--check to see that using this is legal
        if player.mo.skin == "metalsonic" and (player.pflags & PF_JUMPED) and not (player.pflags & PF_THOKKED) then
			--store dashmode early and remove your dashmode stat to make sure that stuff actually works
			player.dashcharge = player.dashmode
			player.dashmode = 0
			--removes metal's dashmode jank physics until they land
			player.charflags = $ &~SF_DASHMODE
			--removes metal's tank powers until they land
			player.powers[pw_strong] = $ &~STR_DASH
			--sets metal's skin values to the normal values
			player.jumpfactor = 1*FRACUNIT
			player.normalspeed = 36*FRACUNIT
			player.acceleration = 40
			player.accelstart = 96
			player.thrustfactor = 5
			--more checks to make sure you cant fuckin hit anything until you land (hardcode resets these on landing/next jumping)
			player.pflags = $|PF_THOKKED|PF_JUMPED|PF_NOJUMPDAMAGE
			--vfx: play the death egg rocket launch sound and put you in falling state
			S_StartSound(player.mo, sfx_s3k82)
			player.mo.state = S_PLAY_FALL
			--set msconv to 1, allowing release of double jump for upwards momentum halving
			player.msconv = 1
			--activate equivalent exchange when you have dashmode: kills your dashmode, converts excess dashmode to jump height, plays extra lightning sfx
			if player.dashcharge > 104 then
				player.dashmode = 0
				P_SetObjectMomZ(player.mo, (player.dashcharge*2/13)*FRACUNIT, false)
				S_StartSound(player.mo, sfx_s3k85)
				player.eeghosts = 1
				player.mtlghosttimer = 4
				player.mtlghostcount = 6
			--otherwise do a tiny baby hop :3
			else
				player.dashmode = 0
				P_SetObjectMomZ(player.mo, (player.dashcharge*2/17)*FRACUNIT, false)
			end
			player.dashmode = 0
			if player.mo.momx and player.mo.momy then
				--hypotenuse takes vector sum of X and Y momenta, function then launches you w/ same momentum in your current facing angle
				P_InstaThrust(player.mo, player.mo.angle, min(2359296,FixedHypot(player.mo.momx, player.mo.momy)))
			end
			player.dashmode = 0
		end
end)