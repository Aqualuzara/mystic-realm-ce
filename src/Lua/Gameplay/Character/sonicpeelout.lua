--original peelout animation mod by SuperPhanto
freeslot("SPR_PEEL")

addHook("ThinkFrame", function()
	for player in players.iterate do
		if (player and player.mo and player.mo.valid) and (player.mo.skin == "sonic") then
			if player.mo.state == S_PLAY_DASH
			and not player.powers[pw_super] and not (player.solchar and player.solchar.istransformed) and not (player.mo.eflags & MFE_FORCESUPER) then
				for i = -40, 40 do

					local force = i*FRACUNIT/3
					local angle = ANGLE_90

					local shiftx = FixedMul(cos(player.drawangle + angle), force)
					local shifty = FixedMul(sin(player.drawangle + angle), force)

					local shiftx2 = FixedMul(cos(player.drawangle), FRACUNIT)
					local shifty2 = FixedMul(sin(player.drawangle), FRACUNIT)

					if i > 20
					or i < -20 then
						local peelout = P_SpawnMobjFromMobj(player.mo, shiftx2 + shiftx, shifty2 + shifty, 0, MT_OVERLAY)
						peelout.target = player.mo
						peelout.fuse = 1
						peelout.sprite = SPR_PEEL
						if i < 0 and player.mo.frame <= D then
							peelout.frame = player.mo.frame
							peelout.angle = player.drawangle + ANGLE_90/6
						else
							if player.mo.frame <= B then
								peelout.frame = player.mo.frame + 2
							else
								peelout.frame = player.mo.frame - 2
							end
							peelout.angle = player.drawangle - ANGLE_90/6
						end
						peelout.renderflags = RF_PAPERSPRITE
						peelout.scale = player.mo.scale/2
						if player.mrce.glowaura then
							peelout.colorized = true
							peelout.color = player.skincolor
							peelout.blendmode = player.mo.blendmode
						end
						if player.mo.eflags & MFE_VERTICALFLIP then
							peelout.eflags = $ | MFE_VERTICALFLIP
							peelout.z = player.mo.z + player.mo.height - peelout.height
						end
					end
				end
			end
		end
	end
end)