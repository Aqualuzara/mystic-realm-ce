local emerald = {EMERALD1, EMERALD2, EMERALD3, EMERALD4, EMERALD5, EMERALD6, EMERALD7}

local settings_multi_timers = {
	0,
	12*TICRATE,
	8*TICRATE,
	5*TICRATE,
	3*TICRATE,
	TICRATE,
	15,
	7
}
local multipliers = { -- Could have been done with bitshifting easily, but I rather have control.
	[0] = 0, 1, 2, 4, 8, 16, 32, 64, 128
}

local emstagerank = 1
local dropped = 0
local emevaluated = false

rawset(_G, "mrce_emmystagerank", emstagerank)

-- HUD

local rotational_time = 180/3
local mp_rot_timer = 0

local reached_goal_val = false
local reached_token_val = false
local emerald_flash_timer = 0

local debug_mode = false

-- GAME

local last_multiplier = 1
local multiplier = 1
local multiplier_timer = 0

local freeze_time = 8*TICRATE
local multiplier_freeze = 0

local global_points = 0

local scorereq = 0
local tokenreq = 0

COM_AddCommand("mrce_emeraldstagedebug", function()
	debug_mode = not($)
end)

addHook("MapLoad", function()
	if multiplayer then
		global_points = 0
		multiplier_freeze = 0
		multiplier_timer = 0
		multiplier = 1
	end
	emevaluated = false

	scorereq = tonumber(mapheaderinfo[gamemap].mrce_emeraldscore or 0)
	tokenreq = tonumber(mapheaderinfo[gamemap].mrce_emeraldtoken or 0)

	local num = tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)

	if emerald[num] then
		emeralds = $|emerald[num]
	end
end)

addHook("PlayerSpawn", function(p)
	if not (p.starpostnum or p.bot or multiplayer) then
		global_points = 0
		multiplier_freeze = 0
		multiplier_timer = 0
		multiplier = 1
	end

	if not p.mrce_emeraldstageinitscore then
		p.mrce_emeraldstageinitscore = p.score
		p.mrce_emeraldstageinitrings = p.rings
	end
end)

addHook("ThinkFrame", function()
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	mrce_emmystagerank = emstagerank

	if multiplier_freeze then
		multiplier_freeze = $-1
	else
		if multiplier_timer then
			multiplier_timer = $-1
			if multiplier > 0 and not multiplier_timer then
				multiplier = $-1
				multiplier_timer = settings_multi_timers[multiplier]
			end
		end
	end
end)

local playerdamagedlevel = false
local emeraldmap = 0

addHook("MobjDamage", function(mo)
	if not dropped then dropped = gamemap end
end, MT_PLAYER)

addHook("MapLoad", function(map)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then
		emeraldmap = 0
		dropped = 0
		return
	end
	if emeraldmap == 0 then
		emeraldmap = gamemap
	elseif emeraldmap == gamemap then
		dropped = gamemap
	else
		dropped = 0
	end
	if playerdamagedlevel then playerdamagedlevel = false end
end)

addHook("PlayerThink", function(p)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if not (p.mo and p.mo.valid) then return end

	local num = tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)

	if p.mrce_emeraldstageinitscore ~= p.score then
		local dif = max(p.score - p.mrce_emeraldstageinitscore, 0)
		global_points = $+dif*multipliers[multiplier]*2
		multiplier_timer = min(multiplier_timer+dif/10, settings_multi_timers[multiplier])
		p.mrce_emeraldstageinitscore = p.score
	end

	if p.mrce_emeraldstageinitrings ~= p.rings then -- will likely need some work considering there is exploit with getting rings from hurted player.
		local dif = max(p.rings - p.mrce_emeraldstageinitrings, 0)
		local scoregain = 200*dif
		global_points = $+scoregain*multipliers[multiplier]
		multiplier_timer = min(multiplier_timer+scoregain/9, settings_multi_timers[multiplier])
		p.mrce_emeraldstageinitrings = p.rings
	end

	if emerald[num] and not (multiplayer or p.bot or (p.mrce_emeraldstageflyingemerald and p.mrce_emeraldstageflyingemerald.valid)) then
		p.mrce_emeraldstageflyingemerald = P_SpawnMobjFromMobj(p.mo, 0, 0, 0, MT_FAKEEMERALD1)
		p.mrce_emeraldstageflyingemerald.flags = $|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING|MF_NOBLOCKMAP &~ MF_SPECIAL
		p.mrce_emeraldstageflyingemerald.scale = 4*p.mo.scale/5
		p.mrce_emeraldstageflyingemerald.state = S_FAKEEMERALD1 + (num - 1)
		p.mrce_emeraldstageflyingemerald.target = p.mo
		p.mrce_emeraldstageflyingemerald.emeraldchase = true
	end

	if mapheaderinfo[gamemap].mrce_emeraldscore and mapheaderinfo[gamemap].mrce_emeraldtoken and mapheaderinfo[gamemap].mrce_emtime then
		local cscore = tonumber(mapheaderinfo[gamemap].mrce_emeraldscore)
		local ascore = tonumber(mapheaderinfo[gamemap].mrce_emeraldtoken)
		if not (p.pflags & PF_FINISHED) then
			if global_points < cscore / 2 then
				emstagerank = 1
			elseif global_points < cscore then
				emstagerank = 2
			elseif global_points < (((ascore - cscore) / 2) + cscore) then
				emstagerank = 3 --good job, a super emerald for you
			elseif global_points < ascore then
				emstagerank = 4
			else
				emstagerank = 5
			end
		elseif not multiplayer then --no S ranks in coop
			if p.realtime <= tonumber(mapheaderinfo[gamemap].mrce_emtime)*TICRATE and not dropped and not emevaluated then
				emstagerank = $ + 1
				emevaluated = true
			end
			--print(dropped .. " " .. emevaluated)
		end
	end

	if (p.pflags & PF_FINISHED) and ((scorereq and global_points >= scorereq and num < 8) or not scorereq) then -- TEMP SOLUTION
		GlobalBanks_Array[0] = $|(1 << (num - 1))
	end
end)


--
--
--
--
--  HUD
--
--
--
--



-- {offx, offy, offscale_x, offscale_y, sprite, nexts}

local function pad(str, num, symb)
	local dif = num - str:len()
	local result = str
	if dif > 0 then
		result = string.rep(symb, dif)..result
	end
	return result
end

local color_table = {
	SKINCOLOR_SAPPHIRE,
	SKINCOLOR_LAVENDER,
	SKINCOLOR_APPLE,
	SKINCOLOR_SALMON,
	SKINCOLOR_ARCTIC,
	SKINCOLOR_SUNSET,
	SKINCOLOR_BONE,

	SKINCOLOR_GREY,
}

local rotateringhud = 0
local last_recordedHUDrings = 0
local ring_outline = {0,-1,0,1,1,0,-1,0}

freeslot("SKINCOLOR_FULLHUDRAINBOW")

local color_ramp = {189, 74, 65, 54, 59, 39, 204, 194, 165, 150, 149, 148, 132, 123, 100, 98}

skincolors[SKINCOLOR_FULLHUDRAINBOW] = {
	accessible = false,
	ramp = color_ramp,
}

customhud.SetupItem("mrce_specialstage", "mrce", function(v, p, c, e)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then
		return
	end

	if debug_mode then
		local debug_y = 192

		v.drawString(2, debug_y, string.format("Freeze_time: %d/%d", multiplier_freeze, freeze_time), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("mutliplier_timer: %d/%d", multiplier_timer, settings_multi_timers[multiplier]), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("score_requirement: %d/%d/%d", global_points, scorereq, tokenreq), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("last_values (multi, rings): %d, %d", last_multiplier, last_recordedHUDrings), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, "Current Rank: " .. tostring(emstagerank), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
	end


	local curcolor = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
	local colormap = v.getColormap(TC_DEFAULT, curcolor)
	v.draw(160, 22, v.cachePatch("EMHUD_BG"), V_SNAPTOTOP, colormap)

	if multiplier_freeze then
		local progress = multiplier_freeze*FRACUNIT/freeze_time
		local patch = v.cachePatch("EMHUD_BGF")
		local crop_left = ease.linear(progress, patch.width/2, 0) << FRACBITS
		local crop_right = ease.linear(progress, 0, patch.width) << FRACBITS

		v.drawCropped(160 << FRACBITS + crop_left, 22 << FRACBITS, FRACUNIT, FRACUNIT, patch, V_SNAPTOTOP, colormap, crop_left, 0, crop_right, patch.height << FRACBITS)
	end


	if multiplier_timer then
		local progress = (((multiplier_timer)*FRACUNIT/settings_multi_timers[multiplier])*109)/FRACUNIT
		--v.drawFill(230, 30, progress+1, 2, 152|V_SNAPTOTOP|V_SNAPTORIGHT)
		local first_pick = skincolors[curcolor].ramp[2]
		v.drawFill(148-progress, 32, progress+1, 2, first_pick|V_SNAPTOTOP)
		v.drawFill(172, 32, progress+1, 2, first_pick|V_SNAPTOTOP)
	end

	local em_meter_x = 159
	v.draw(em_meter_x, 22, v.cachePatch("EMEMERMET0"), V_SNAPTOTOP)

	if scorereq then
		local scorenum = global_points or 1
		local progress = max(min(FixedDiv(scorenum, scorereq), FRACUNIT), 0)
		local patch_1 = v.cachePatch("EMEMERMET1")
		local act_size = patch_1.height * FRACUNIT
		local dif = act_size - progress*patch_1.height



		if progress == FRACUNIT and not reached_goal_val then
			reached_goal_val = true
			emerald_flash_timer = FRACUNIT
		elseif progress < FRACUNIT then
			reached_goal_val = false
		end

		--print(string.format('%f', progress))

		v.drawCropped(em_meter_x << FRACBITS, 22 << FRACBITS + dif, FRACUNIT, FRACUNIT, patch_1, V_SNAPTOTOP, colormap, 0, abs(dif), patch_1.width * FRACUNIT, act_size)

		if tokenreq then
			progress = max(min(FixedDiv(max(scorenum - scorereq, 1), tokenreq), FRACUNIT), 0)
			local patch_2 = v.cachePatch("EMEMERMET2")
			local rainbowc = v.getColormap(TC_DEFAULT, SKINCOLOR_FULLHUDRAINBOW)

			if progress == FRACUNIT and not reached_token_val then
				reached_token_val = true
				emerald_flash_timer = FRACUNIT
			elseif progress < FRACUNIT then
				reached_token_val = false
			end

			act_size = patch_2.height * FRACUNIT
			dif = act_size - progress*patch_2.height

			v.drawCropped(em_meter_x << FRACBITS, 22 << FRACBITS + dif, FRACUNIT, FRACUNIT, patch_2, V_SNAPTOTOP, rainbowc, 0, abs(dif), patch_2.width * FRACUNIT, act_size)

			-- Color scrolling
			local first_index = color_ramp[1]
			table.remove(color_ramp, 1)
			table.insert(color_ramp, first_index)
			skincolors[SKINCOLOR_FULLHUDRAINBOW].ramp = color_ramp
		end


		if emerald_flash_timer then
			local color = colormap

			if reached_token_val then
				color = v.getColormap(TC_DEFAULT, SKINCOLOR_FULLHUDRAINBOW)
			end

			local transparency = ease.insine(emerald_flash_timer, 0, 10) << V_ALPHASHIFT

			v.drawScaled(em_meter_x << FRACBITS, 22 << FRACBITS, FRACUNIT + emerald_flash_timer/4, v.cachePatch("EMEMERMET3"), V_SNAPTOTOP|transparency, colormap)

			emerald_flash_timer = emerald_flash_timer/3
		end
	end

	--v.drawLevelTitle(160 + 16*cos((3*leveltime % 180)*ANG1)/FRACUNIT, 22, multipliers[multiplier], V_SNAPTOTOP)

	if last_multiplier ~= multiplier then
		if last_multiplier < multiplier then
			mp_rot_timer = rotational_time
		else
			mp_rot_timer = -rotational_time
		end
		last_multiplier = multiplier
	end

	local time_rp = mp_rot_timer + 28
	local nangle = ((time_rp + 60) % 180)*ANG1
	MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 15 * FRACUNIT, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..multiplier), V_SNAPTOTOP|V_20TRANS)


	if mp_rot_timer then
		if multiplier > 1 then
			nangle = ((time_rp) % 180)*ANG1
			MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 15 * FRACUNIT, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..(multiplier-1)), V_SNAPTOTOP|V_20TRANS)
		end


		if multiplier < #multipliers then
			nangle = ((time_rp + 120) % 180)*ANG1
			MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 15 * FRACUNIT, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..(multiplier+1)), V_SNAPTOTOP|V_20TRANS)
		end
	end

	TBSlib.fontdrawerInt(v, "MRCEHU2FNT", 26, 21, tostring(global_points), V_SNAPTOTOP, nil, "left", -1, 9, '0')

	if mp_rot_timer then
		mp_rot_timer = mp_rot_timer/3
	end





	local getrings = p.mo.skin == "xian" and p.xian.rings or p.rings

	if last_recordedHUDrings ~= getrings then
		if getrings > last_recordedHUDrings then
			rotateringhud = $+abs(last_recordedHUDrings - getrings)*24
		end
		last_recordedHUDrings = getrings
	end

	local ringsfont = getrings > 0 and 'MRCEHUDFNT' or ((leveltime % 12)/6 and 'MRCEHURFNT' or 'MRCEHUDFNT')
	local ringcolor = p.mo.pw_combine and v.getColormap(TC_DEFAULT, 0, 'PurpleRingTranslation') or nil

	rotateringhud = max(ease.linear(FRACUNIT/12, rotateringhud, 0), 0)
	-- Rings
	local x = 21
	local y = 29

	local ringtimer = (rotateringhud) % 23
	local ring = v.getSpritePatch(SPR_RING, ringtimer)
	local flip = (ringtimer/12) and V_FLIP or 0
	local color = v.getColormap(TC_ALLWHITE, 31)
	local xwidth = ring.width/4
	local xheight = ring.height/2

	for i = 2, 8, 2 do
		local xoff = ring_outline[i-1]
		local yoff = ring_outline[i]
		v.drawScaled((x+xoff+xwidth) << FRACBITS, (y+yoff+xheight) << FRACBITS, FRACUNIT/3, ring, V_SNAPTOTOP|flip, color)
	end
	v.drawScaled((x+xwidth) << FRACBITS, (y+xheight) << FRACBITS, FRACUNIT/3, ring, V_SNAPTOTOP|flip, ringcolor)

	TBSlib.fontdrawerInt(v, "MRCEHU2FNT", 40-2, 34, tostring(getrings), V_SNAPTOTOP, nil, "left", -1, 4, '0')

	TBSlib.fontdrawerInt(v, "MRCEHU2FNT", 301, 21, pad(tostring(G_TicsToMinutes(p.realtime)), 2, '0')..':'..pad(tostring(G_TicsToSeconds(p.realtime)), 2, '0')..':'..pad(tostring(G_TicsToCentiseconds(p.realtime)), 2, '0'), V_SNAPTOTOP, nil, "right", -1, 3, '0')

	x = 297
	y = 44
	local skin_color = v.getColormap(p.skin, p.skincolor)

	for cont = 1, min(p.continues + 1, 8) do
		local continuepatch = v.getSprite2Patch(p.skin, SPR2_XTRA, C, 0)
		v.drawScaled(x << FRACBITS, y << FRACBITS, FRACUNIT/2-FRACUNIT/8, continuepatch, V_SNAPTOTOP, skin_color)
		cont = $ - continuepatch.width/4 - 2
	end
end, "game")

local message_table = {
	[3] = "ESCAPE ILLUSION FOREST",
	[4] = "!RUN AWAY!",
}

customhud.SetupItem("stagetitle_specialstage_mrce", "mrce", function(v, p, c, e)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then
		--customhud.enable("stagetitle")
		return false
	end

	if t > 25 and t < 49 then
		v.fadeScreen(0, 10 - 3*abs((t-25) % 24 - 12)/4)
	end

	--customhud.disable("stagetitle")
end, "titlecard")




--
--
--
--
--  POWER UPS
--
--
--
--




local multi_crystal = freeslot("MT_MRCEMULTICRYSTAL")
freeslot("S_MRCEMULTICRYSTAL", "SPR_EMST")

states[S_MRCEMULTICRYSTAL] = {
	sprite = SPR_EMST,
	frame = A|FF_SEMIBRIGHT|FF_ANIMATE,
	tics = 12,
	var1 = 3,
	var2 = 3,
	nextstate = S_MRCEMULTICRYSTAL,
}

mobjinfo[multi_crystal] = {
--$Category Emerald Stages
--$Name Multi Crystal
--$Sprite EMSTA0
--$NotAngled
        doomednum = 1481,
        spawnstate = S_MRCEMULTICRYSTAL,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 11*FRACUNIT,
        height = 47*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

addHook("MobjSpawn", function(mo)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then P_RemoveMobj(mo) end
	if not color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)] then return end
	mo.color = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
end, multi_crystal)

local ScaleDecrement = FRACUNIT/16

local Crystal_Pickup = {
	[0] = {offscale_x = 0, offscale_y = 0, tics = 1, nexts = 1, waittill = function(mo, anim)
		MRCElibs.lerpMoveObjectToPos(mo, FRACUNIT/8+mo.speed, mo.target.x, mo.target.y, mo.target.z)
		local dist = R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y)

		if dist < mo.radius then
			return true
		end
	end},
	[1] = {offscale_x = 0, offscale_y = 0, tics = 1, nexts = 2, waittill = function(mo, anim)
		local tar = mo.target
		if not anim.newtics then
			anim.newtics = 1
		end

		anim.newtics = $+1
		local ang = anim.newtics*ANG20


		P_MoveOrigin(mo, tar.x + FixedMul(tar.radius, sin(ang+tar.angle)),
		tar.y + FixedMul(tar.radius, cos(ang+tar.angle)), tar.z+tar.height/8)
		mo.scale = max(mo.scale-ScaleDecrement, 8)

		if anim.newtics > TICRATE/2 then
			mo.extravalue1 = 1
			return true
		end
	end},
	[2] = {offscale_x = 0, offscale_y = 0, tics = 4, nexts = 0}, -- dummy state
}

addHook("MobjThinker", function(mo)
	if not mo.target then
		TBSlib.resetAnimator(mo)
		IsPlayerAround(mo, FRACUNIT*256, 8)
		mo.speed = 0
	else
		TBSlib.objAnimator(mo, Crystal_Pickup)
		mo.speed = $+FRACUNIT/32
	end
end, multi_crystal)

addHook("TouchSpecial", function(mo, t)
	if not mo.extravalue1 then
		mo.target = t
		return true
	end

	multiplier = min(multiplier+1, #settings_multi_timers)
	multiplier_timer = settings_multi_timers[multiplier]
	if t.player and t.player.bot then
		P_KillMobj(mo, t)
	end
end, multi_crystal)

addHook("MobjDeath", function(mo, t)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
end, multi_crystal)

addHook("MobjSpawn", function(mo)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if not color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)] then return end
	mo.color = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
	mo.colorized = true
end, MT_HOOP)

addHook("TouchSpecial", function(mo, t)
	if multiplier > 1 then
		multiplier_timer = min($ + settings_multi_timers[multiplier]/2, settings_multi_timers[multiplier])
	end
end, MT_HOOPCOLLIDE)

local deleto_pw = TICRATE*2
local deletotic = deleto_pw/10

local function pw_standardizedbehavior(mo)
	if mo.extravalue1 then
		mo.angle = $+ANG10
		mo.momz = mo.scale*P_MobjFlip(mo)
		mo.scale = $+FRACUNIT/64
		mo.frame = $|((10 - mo.fuse/deletotic) << FF_TRANSSHIFT)
	else
		mo.angle = $+ANG2
	end
end


freeslot("MT_ATTRACTIONEMPW", "S_ATTRACTIONEMPW")

mobjinfo[MT_ATTRACTIONEMPW] = {
--$Category Emerald Stages
--$Name Attraction Power Up
--$Sprite EMSTG0
--$NotAngled
        doomednum = 1482,
        spawnstate = S_ATTRACTIONEMPW,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[S_ATTRACTIONEMPW] = {
	sprite = SPR_EMST,
	frame = G|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		P_SwitchShield(t.player, SH_ATTRACT)
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, MT_ATTRACTIONEMPW)

addHook("MobjThinker", pw_standardizedbehavior, MT_ATTRACTIONEMPW)

local freeze_time_mb = freeslot("MT_FREEZETIMEREMPW")
local freeze_time_st = freeslot("S_FREEZETIMEREMPW")

mobjinfo[freeze_time_mb] = {
--$Category Emerald Stages
--$Name Time Freeze Power Up
--$Sprite EMSTH0
--$NotAngled
        doomednum = 1483,
        spawnstate = freeze_time_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[freeze_time_st] = {
	sprite = SPR_EMST,
	frame = H|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		multiplier_freeze = $+freeze_time
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, freeze_time_mb)

addHook("MobjThinker", pw_standardizedbehavior, freeze_time_mb)

local armapw_mb = freeslot("MT_ARMAGEDDONEMPW")
local armapw_st = freeslot("S_ARMAGEDDONEMPW")

mobjinfo[armapw_mb] = {
--$Category Emerald Stages
--$Name Armageddon Power Up
--$Sprite EMSTI0
--$NotAngled
        doomednum = 1484,
        spawnstate = armapw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[armapw_st] = {
	sprite = SPR_EMST,
	frame = I|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		P_SwitchShield(t.player, SH_ARMAGEDDON)
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, armapw_mb)

addHook("MobjThinker", pw_standardizedbehavior, armapw_mb)

local scorepw_mb = freeslot("MT_SCOREEMPW")
local scorepw_st = freeslot("S_SCOREEMPW")

mobjinfo[scorepw_mb] = {
--$Category Emerald Stages
--$Name Score Up
--$Sprite EMSTJ0
--$Arg0 Custom Score Value
--$NotAngled
        doomednum = 1485,
        spawnstate = scorepw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[scorepw_st] = {
	sprite = SPR_EMST,
	frame = J|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("MapThingSpawn", function(mo, ma)
	mo.extravalue2 = max(ma.args[0], 0)	-- scoreadd
end, scorepw_mb)

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		global_points = $ + (mo.extravalue2 or 1000)
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, scorepw_mb)

addHook("MobjThinker", pw_standardizedbehavior, scorepw_mb)

local timescorepw_mb = freeslot("MT_TIMESCOREEMPW")
local timescorepw_st = freeslot("S_TIMESCOREEMPW")

mobjinfo[timescorepw_mb] = {
--$Category Emerald Stages
--$Name Timed Score Up
--$Sprite EMSTK0
--$Arg0 Regressing Threshold (1s = 35 tics)
--$Arg1 Regression Length (1s = 35 tics)
--$Arg2 Custom Score Value
--$NotAngled
        doomednum = 1486,
        spawnstate = timescorepw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[timescorepw_st] = {
	sprite = SPR_EMST,
	frame = K|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("MapThingSpawn", function(mo, ma)
	mo.cusval = max(ma.args[0], 0) -- threshold
	mo.reactiontime = max(ma.args[1], 0)	-- regresing
	mo.extravalue2 = max(ma.args[2], 0)	-- scoreadd
end, timescorepw_mb)


addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		if mo.cusval then
			local time_adj = min(max(leveltime - mo.cusval, 0) * FRACUNIT/(mo.reactiontime or 256), FRACUNIT)
			global_points = $ + max(ease.linear(time_adj, mo.extravalue2 or 1000, 0), 0)*multipliers[multiplier]
		else
			global_points = $ + (mo.extravalue2 or 1000)*multipliers[multiplier]
		end
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, timescorepw_mb)

addHook("MobjThinker", pw_standardizedbehavior, timescorepw_mb)

local bombpw_mb = freeslot("MT_BOMBEMPW")
local bombpw_st = freeslot("S_BOMBEMPW")

mobjinfo[bombpw_mb] = {
--$Category Emerald Stages
--$Name Bomb Power Up
--$Sprite EMSTL0
--$Arg0 Radius
--$Arg0Type 23
--$NotAngled
        doomednum = 1487,
        spawnstate = bombpw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[bombpw_st] = {
	sprite = SPR_EMST,
	frame = L|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("MapThingSpawn", function(mo, ma)
	mo.radius = max(ma.args[0] * FRACUNIT, 0) -- threshold
end, bombpw_mb)

local function bomb_surrondings(ref, mo)
	if mo.flags & MF_ENEMY then
		ref.extravalue2 = $ + (ref.extravalue2/2 or 100)
		P_KillMobj(mo)
	elseif mo.type == MT_EGGMAN_BOX
	or mo.type == MT_SPIKEBALL
	or mo.type == MT_WALLSPIKE
	or mo.type == MT_SPIKE then
		ref.extravalue2 = $ + 200
		P_KillMobj(mo)
	end
end

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		searchBlockmap("objects", bomb_surrondings, mo)
		if mo.extravalue2 then
			global_points = $ + mo.extravalue2*multipliers[multiplier]
		end
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, bombpw_mb)

addHook("MobjThinker", pw_standardizedbehavior, bombpw_mb)

hud.add(function(v)
	if not mapheaderinfo[gamemap].mrce_emeraldstage or tonumber(mapheaderinfo[gamemap].mrce_emeraldstage) > 7 then return end
	if not (mapheaderinfo[gamemap].mrce_emeraldscore and mapheaderinfo[gamemap].mrce_emeraldtoken and mapheaderinfo[gamemap].mrce_emtime) then return end
	local grade = "GRADE" .. tostring(emstagerank)
	v.draw(160, 160, v.cachePatch(grade), V_SNAPTOBOTTOM)
end, "intermission")


addHook("NetVars", function(net)
	global_points = net($)

	multiplier = net($)
	multiplier_timer = net($)
	multiplier_freeze = net($)
	emstagerank = net($)
	dropped = net($)
end)