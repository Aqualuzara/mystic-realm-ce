freeslot("MT_AIZSNAKEPAIN", "MT_AIZSNAKE", "MT_AIZSNAKETAIL", "S_AIZSNAKE", "SPR_2AIZ", "SPR_AEN2")

--	C Translation of Dragon Fly thinker
--	With changes
local function P_CatterkillerJRThink(mobj)
	local DRAGONTURNSPEED = ANG2
	mobj.movecount = (mobj.movecount + 9) % 360
	P_SetObjectMomZ(mobj, 3*sin(mobj.movecount*ANG1), false)

	if (mobj.target) then -- Are we chasing a player?
		local dist = P_AproxDistance(mobj.x - mobj.target.x, mobj.y - mobj.target.y)
		if (dist > 1200*mobj.scale) then -- Not anymore!
			mobj.target = nil
		else
			local vspeed = FixedMul(mobj.info.speed >> 3, mobj.scale)
			local z = mobj.target.z + (mobj.height >> 1) + (mobj.flags & MFE_VERTICALFLIP and mobj.scale or mobj.scale + mobj.target.height)
			local diff = R_PointToAngle2(mobj.x, mobj.y, mobj.target.x, mobj.target.y) - mobj.angle
			if (diff > ANGLE_180) and (diff < ANGLE_MAX) then
				mobj.angle = $ - DRAGONTURNSPEED
			else
				mobj.angle = $ + DRAGONTURNSPEED
			end
			if not mobj.threshold and dist < 512*mobj.scale then -- Close enough to drop bombs
				mobj.threshold = mobj.info.painchance
			end
			mobj.momz = $ + max(min(z - mobj.z, vspeed), -vspeed)
		end
	else 		-- Can we find a player to chase?
		if mobj.target == nil then
			P_LookForPlayers(mobj, 1200*mobj.scale, true, false) -- if not, circle around the spawnpoint
		end
		if not (mobj.spawnpoint) then -- unless we don't have one, in which case uhhh just circle around wherever we currently are I guess??
			mobj.angle = $ + DRAGONTURNSPEED
		else
			local flip = mobj.spawnpoint.options & MTF_OBJECTFLIP
			local vspeed = FixedMul(mobj.info.speed >> 3, mobj.scale)
			local x = mobj.spawnpoint.x << FRACBITS
			local y = mobj.spawnpoint.y << FRACBITS
			local z = (flip and mobj.ceilingz or mobj.floorz) + (flip and -1 or 1)*(mobj.spawnpoint.z << FRACBITS)
			local diff = R_PointToAngle2(mobj.x, mobj.y, x, y) - mobj.angle
			if (diff > ANGLE_180) then
				mobj.angle = $ - DRAGONTURNSPEED
			else
				mobj.angle = $ + DRAGONTURNSPEED
			end
			mobj.momz = $ + max(min(z - mobj.z, vspeed), -vspeed)
		end
	end
	P_InstaThrust(mobj, mobj.angle, FixedMul(mobj.info.speed, mobj.scale))
end

-- Free to be edited,
-- C-translated A_DragonSegment
local function P_SnakeSegment(a, order)
	local t = a.target
	local dist, radius, hangle, zangle, hdist, xdist, ydist, zdist;

	if not (a and a.valid) then return end

	if not (a.origin or a.origin.valid or a.origin.health) then
		a.flags = $|MF_NOCLIPTHING &~ MF_PAIN
	end

	if t and t.health and t.health > 0 then
		dist = P_AproxDistance(P_AproxDistance(a.x - t.x, a.y - t.y), a.z - t.z)
		radius = (a.radius << 1) + (t.type == MT_AIZSNAKE and (t.radius >> 1) or (t.radius << 1))
		hangle = R_PointToAngle2(t.x, t.y, a.x, a.y)
		zangle = R_PointToAngle2(0, t.z, dist, a.z)
		hdist = P_ReturnThrustX(t, zangle, radius)
		xdist = P_ReturnThrustX(t, hangle, hdist)
		ydist = P_ReturnThrustY(t, hangle, hdist)
		zdist = P_ReturnThrustY(t, zangle, radius)

		a.angle = hangle
		P_MoveOrigin(a, t.x + xdist, t.y + ydist, t.z + zdist)
	else
		if a.extravalue1 then
			a.extravalue1 = $-1
		else
			if a and a.health and a.health > 0 then
				P_KillMobj(a)
				a.spriteyoffset = $-(FRACUNIT >> 2)*3
				a.scale = a.scale-(order*(FRACUNIT >> 4))
			end
		end
	end
end


mobjinfo[MT_AIZSNAKE] = {
--$Category Mystic Realm Enemies
--$Name FRZ Catakiller Goth.
--$Sprite AEN2A1
	doomednum = 2511,
	spawnstate = S_AIZSNAKE,
	spawnhealth = 1,
	reactiontime = 0,
	painchance = 6,
	deathstate = S_XPLD_FLICKY,
	deathsound = sfx_pop,
	radius = 20*FRACUNIT,
	height = 40*FRACUNIT,
	speed = 5*FRACUNIT,
	mass = 100,
	flags = MF_SPECIAL|MF_SHOOTABLE|MF_ENEMY|MF_NOGRAVITY|MF_BOUNCE|MF_RUNSPAWNFUNC
}

mobjinfo[MT_AIZSNAKETAIL] = {
	spawnstate = S_AIZSNAKE,
	spawnhealth = 1,
	reactiontime = 8,
	deathstate = S_XPLD1,
	deathsound = sfx_pop,
	radius = 6*FRACUNIT,
	height = 16*FRACUNIT,
	mass = 100,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SLIDEME|MF_PAIN
}

mobjinfo[MT_AIZSNAKEPAIN] = {
	spawnstate = S_AIZSNAKE,
	spawnhealth = 1,
	reactiontime = 8,
	deathstate = S_XPLD1,
	deathsound = sfx_pop,
	radius = 4*FRACUNIT,
	height = 8*FRACUNIT,
	mass = 100,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SLIDEME|MF_PAIN
}

states[S_AIZSNAKE] = {
	sprite = SPR_AEN2,
	frame = A
}

addHook("MapThingSpawn", function(a, tm)
	a.snake = {}

	for i = 1,6 do
		local angt = (tm.angle*ANG1)
		local button = P_SpawnMobjFromMobj(a, -(12*cos(angt))*i,-(12*sin(angt))*i,0, MT_AIZSNAKETAIL)
		button.state = S_INVISIBLE
		button.angle = angt
		button.sprite = SPR_AEN2
		button.extravalue1 = (i % 2 and 3 or 0)
		button.extravalue2 = i
		button.target = (i == 1 and a or a.snake[i-1])
		button.origin = a
		if i == 1 then
			button.flags = $ &~ MF_PAIN
		end
		if i > 3 and i < 6 then
			button.frame = C
		elseif i == 6 then
			button.frame = D
		else
			button.frame = B
		end
		table.insert(a.snake, button)
	end
end, MT_AIZSNAKE)

addHook("MobjThinker", P_CatterkillerJRThink, MT_AIZSNAKE)

addHook("MobjDeath", function(a)
	a.spriteyoffset = $-FRACUNIT >> 1
end, MT_AIZSNAKE)

addHook("MobjThinker", function(a)
	local id = a.extravalue2 or 0

	P_SnakeSegment(a, id)
	local idt = id*2
		
		
	local isPlayerNear
	
	for aPlayer in players.iterate
		if aPlayer.valid and aPlayer then
			if R_PointToDist2(aPlayer.mo.x, aPlayer.mo.y, a.x, a.y) < 2500*FRACUNIT then
				isPlayerNear = true
			else
				isPlayerNear = false
			end
		end
	end
	
	if isPlayerNear == false
		return
	end
	
	
	if a.frame == B and (leveltime % (13+idt))/(11+idt) then
		local angt = a.angle + ANGLE_90
		local fase = P_SpawnMobjFromMobj(a, FixedMul(cos(a.angle + (ANGLE_90+ANGLE_180)), a.radius/2), FixedMul(sin(a.angle + (ANGLE_90+ANGLE_180)), a.radius/2), 12*sin(angt), MT_AIZSNAKEPAIN)
		fase.frame = E|FF_FULLBRIGHT|FF_TRANS10
		fase.angle = a.angle + ANGLE_90+ANGLE_180
		fase.origin = a
		fase.fuse = TICRATE
		fase.speed = a.scale*3
		fase.extravalue1 = fase.fuse
		fase.scale = a.scale
	end
end, MT_AIZSNAKETAIL)

addHook("MobjThinker", function(a)
	a.scale = $+FRACUNIT >> 4
	if a.extravalue1 and a.fuse then
		a.frame = (ease.insine(a.fuse*(FRACUNIT/a.extravalue1), J, E)+1)|FF_FULLBRIGHT|FF_TRANS10

		if a.fuse <= TICRATE/4 then
			a.flags = $ &~ MF_PAIN
			a.dispoffset = 8
		end
	end
	if not (a.origin and a.origin.health) then
		a.fuse = min(TICRATE/4-1, a.fuse)
		a.flags = $ &~ MF_PAIN
	elseif a.fuse > 2*TICRATE/3 then
		a.momx = a.origin.momx
		a.momy = a.origin.momy
		a.momz = a.origin.momz + a.speed/2
	else
		P_InstaThrust(a, a.angle, a.speed)
		a.momz = a.speed
	end
end, MT_AIZSNAKEPAIN)