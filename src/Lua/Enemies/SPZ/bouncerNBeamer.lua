freeslot(
	"S_LASERDUDE_LASER1",
	"S_LASERDUDE_LASER2",
	"S_LASERDUDE_LASER4",
	"S_LASERTARGET_STAND",
	"S_LASERTARGET_CHASE1",
	"S_LASERTARGET_CHASE2",
	"S_LASERTARGET_CHASE3",
	"S_LASERTARGET_CHASE4",
	"S_LASERTARGET_CHASE5",
	"S_LASERTARGET_CHASE6",
	"S_LASERTARGET_CHASE7",
	"S_LASERTARGET_RESET1",
	"S_LASERTARGET_RESET2",
	"S_LASERTARGET_PAIN",
	"S_LASERDUDE_STARTUP1",
	"S_LASERDUDE_STARTUP2",
	"S_LASERDUDE_STARTUP3",
	"SPR_BNCR",
	"SPR_BMER",
	"SPR_OBEM",
	"SPR_OBAL"
)


mobjinfo[MT_LASERTARGET] = {
	--$Name Bouncer
	--$Category Sunken Plant
	--$Sprite BNCRA1
	doomednum = 3006,
	spawnstate = S_LASERTARGET_STAND,
	spawnhealth = 1000,
	speed = 10,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	attacksound = sfx_none,
	activesound = sfx_s1b4,
	painstate = S_LASERTARGET_PAIN,
	painchance = -1,
	painsound = sfx_s3k7b,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_LASERTARGET_PAIN,
	reactiontime = 5,
	seestate = S_LASERTARGET_CHASE1,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SHOOTABLE|MF_BOUNCE|MF_SPRING
}
states[S_LASERTARGET_PAIN] = {
sprite = SPR_BNCR,
frame = A,
action = A_DoNPCPain,
var1 = FRACUNIT,
tics = 1,
nextstate = S_LASERTARGET_STAND
}
states[S_LASERTARGET_STAND] = {
sprite = SPR_BNCR,
frame = A,
action = A_Look,
tics = 1,
nextstate = S_LASERTARGET_STAND
}
states[S_LASERTARGET_CHASE1] = {
sprite = SPR_BNCR,
frame = A,
action = A_Chase,
tics = 4,
nextstate = S_LASERTARGET_CHASE2
}
states[S_LASERTARGET_CHASE2] = {
sprite = SPR_BNCR,
frame = A,
action = A_Chase,
tics = 3,
nextstate = S_LASERTARGET_CHASE3
}
states[S_LASERTARGET_CHASE3] = {
sprite = SPR_BNCR,
frame = A,
action = A_CheckRange,
var1 = 128+128+128+128<<0,
var2 = S_LASERTARGET_CHASE4,
tics = 1,
nextstate = S_LASERTARGET_CHASE1
}
states[S_LASERTARGET_CHASE4] = {
sprite = SPR_BNCR,
frame = C,
action = A_FaceTarget,
var1 = 8,
var2 = 15,
tics = 1,
nextstate = S_LASERTARGET_CHASE5
}
states[S_LASERTARGET_CHASE5] = {
sprite = SPR_BNCR,
frame = B,
action = A_BunnyHop,
var1 = 8,
var2 = 15,
tics = 2,
nextstate = S_LASERTARGET_CHASE6
}
states[S_LASERTARGET_CHASE6] = {
sprite = SPR_BNCR,
frame = A,
action = A_Boss5CheckOnGround,
var1 = S_LASERTARGET_CHASE1,
tics = 1,
nextstate = S_LASERTARGET_CHASE7
}
states[S_LASERTARGET_CHASE7] = {
sprite = SPR_BNCR,
frame = C,
action = A_Repeat,
var1 = 70,
var2 = S_LASERTARGET_CHASE6,
tics = 1,
nextstate = S_LASERTARGET_RESET1
}
states[S_LASERTARGET_RESET1] = {
sprite = SPR_BNCR,
frame = A,
action = A_SetObjectFlags,
var1 = MF_STICKY,
var2 = 1,
tics = 35,
nextstate = S_LASERTARGET_RESET2
}
states[S_LASERTARGET_RESET2] = {
sprite = SPR_BNCR,
frame = A,
action = A_SetObjectFlags,
var1 = MF_STICKY,
var2 = 2,
tics = 1,
nextstate = S_LASERTARGET_CHASE1
}

mobjinfo[MT_LASERDUDE] = {
	--$Name Beamer
	--$Category Sunken Plant
	--$Sprite BMERA1
	--$Arg0 Targetting Mode
	--$Arg0Type 12
	--$Arg0Enum { 1 = "Use tagged Bouncer as target"; }
	--$Arg1Type 14
	--$Arg1 Target Tag
	doomednum = 3005,
	spawnstate = S_LASERDUDE_STARTUP1,
	spawnhealth = 1,
	speed = 3,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	attacksound = sfx_s3k53,
	activesound = sfx_s3k7b,
	painstate = S_NULL,
	painchance = -1,
	painsound = sfx_None,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_NULL,
	reactiontime = 5,
	seestate = S_LASERDUDE_LASER2,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY
}
states[S_LASERDUDE_STARTUP1] = {
sprite = SPR_BMER,
frame = A,
action = none,
tics = 70,
nextstate = S_LASERDUDE_STARTUP2
}

states[S_LASERDUDE_STARTUP2] = {
sprite = SPR_BMER,
frame = A,
action = none,
tics = 10,
nextstate = S_LASERDUDE_STARTUP3
}

states[S_LASERDUDE_STARTUP3] = {
sprite = SPR_BMER,
frame = A,
action = none,
tics = 1,
nextstate = S_LASERDUDE_LASER1
}


states[S_LASERDUDE_LASER1] = {
sprite = SPR_BMER,
frame = A,
action = A_BossJetFume,
var1 = 4,
tics = 35,
nextstate = S_LASERDUDE_LASER2
}

states[S_LASERDUDE_LASER2] = {
sprite = SPR_BMER,
frame = B,
action = A_PlaySound,
var1 = sfx_s3k53,
var2 = 1,
tics = 1,
nextstate = S_LASERDUDE_LASER3
}

states[S_LASERDUDE_LASER3] = {
sprite = SPR_BMER,
frame = C,
action = none,
tics = 35,
nextstate = S_LASERDUDE_LASER4
}

states[S_LASERDUDE_LASER4] = {
sprite = SPR_BMER,
frame = C,
action = none,
tics = 55,
nextstate = S_LASERDUDE_LASER1
}


--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

local function ContinuousLaser(mo, objectType, positionType)
	local x, y, z

	local angle
	local point
	local duration
	local LASERCOLORS =
	{
		SKINCOLOR_LIME
	}


	--Extravalue2 handles the duration


	if not mo.target then
		mo.extravalue2 = 0
	end


	if (mo.extravalue2 > 0) then
		mo.extravalue2 = $ - 1
	end

	duration = mo.extravalue2

	if (positionType == 0) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle+ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle+ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		if (mo.eflags & MFE_VERTICALFLIP) then
			z = mo.z + mo.height - FixedMul(56*FRACUNIT, mo.scale) - mobjinfo[objectType].height
		else
			z = mo.z + FixedMul(56*FRACUNIT, mo.scale)
		end
	elseif (positionType == 1) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle-ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle-ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		if (mo.eflags & MFE_VERTICALFLIP) then
			z = mo.z + mo.height - FixedMul(56*FRACUNIT, mo.scale) - mobjinfo[objectType].height
		else
			z = mo.z + FixedMul(56*FRACUNIT, mo.scale)
		end
	elseif (positionType == 2) then
		--Fire THREE lasers
		ContinuousLaser(mo, objectType, 3)
		ContinuousLaser(mo, objectType, 0)
		ContinuousLaser(mo, objectType, 1)
		return
	elseif (positionType == 3) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle, FixedMul(42*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle, FixedMul(42*FRACUNIT, mo.scale))
		z = mo.z + mo.height/2
	else
		x = mo.x
		y = mo.y
		z = mo.z + mo.height/2
	end


	if ((not (mo.flags2 & MF2_FIRING)) and duration > 1) then

		mo.angle = R_PointToAngle2(x, y, mo.target.x, mo.target.y)
		if (mobjinfo[objectType].seesound) then
			S_StartSound(mo, mobjinfo[objectType].seesound)
		end

		point = P_SpawnMobj(
				x + P_ReturnThrustX(mo, mo.angle, mo.radius),
				y + P_ReturnThrustY(mo, mo.angle, mo.radius),
				mo.z - mo.height / 2, MT_EGGMOBILE_TARGET
			)
		if (point and point.valid) then
			point.angle = mo.angle
			point.fuse = duration+1
			point.target = mo.target
			mo.target = point
			if mo.spawnpoint and mo.spawnpoint.args[0] == 1 and mo.bouncerTarget then
				mo.target = mo.bouncerTarget	
			end
		end	
	end

	angle = R_PointToAngle2(z + (mobjinfo[objectType].height>>1), 0, mo.target.z, R_PointToDist2(x, y, mo.target.x, mo.target.y))

	point = P_SpawnMobj(x, y, z, objectType)
	if (not (point and point.valid)) then
		return
	end

	point.target = mo
	point.angle = mo.angle
	local speed = point.radius
	point.momz = FixedMul(cos(angle), speed)
	point.momx = FixedMul(sin(angle), FixedMul(cos(point.angle), speed))
	point.momy = FixedMul(sin(angle), FixedMul(sin(point.angle), speed))
	point.fuse = duration+1

	local storedmomx = point.momx
	local storedmomy = point.momy

	for i = 0, 255 do
		local spawnedmo = P_SpawnMobj(point.x, point.y, point.z, point.type)
		if (spawnedmo and spawnedmo.valid) then
			spawnedmo.angle = point.angle
			spawnedmo.color = LASERCOLORS[1] --laser cycle

			spawnedmo.flags = MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOGRAVITY|MF_SCENERY

			if ((duration & 1) and spawnedmo.info.missilestate) then

				spawnedmo.state = spawnedmo.info.missilestate
				if (spawnedmo.info.meleestate) then
					local mo2 = P_SpawnMobjFromMobj(spawnedmo, 0, 0, 0, MT_PARTICLE)
					if (mo2 and mo2.valid) then
						mo2.flags2 =  $ | MF2_LINKDRAW
						mo2.tracer = spawnedmo
						mo2.state = spawnedmo.info.meleestate
					end
				end
			end

			if (duration == 1) then
				P_SpawnGhostMobj(spawnedmo)
			end

			x = point.x
			y = point.y
			z = point.z
			if (P_RailThinker(point)) then
				break
			end
		end
	end

	x = $ + storedmomx
	y = $ + storedmomy

	local floorz = P_FloorzAtPos(x, y, z, mobjinfo[MT_EGGMOBILE_FIRE].height)
	if (z - floorz < mobjinfo[MT_EGGMOBILE_FIRE].height>>1 and duration & 1) then

		point = P_SpawnMobj(x, y, floorz, MT_EGGMOBILE_FIRE)
		if (point and point.valid) then

			point.angle = mo.angle
			point.destscale = mo.scale
			P_SetScale(point, point.destscale)
			point.target = mo
			if (point.eflags & (MFE_UNDERWATER|MFE_TOUCHWATER)) then
				for i = 0, 1 do
					local size = 3
					local steam = P_SpawnMobj(x, y, point.watertop - size*mobjinfo[MT_DUST].height, MT_DUST)
					if (steam and steam.valid) then
						P_SetScale(steam, size*mo.scale)
						P_SetObjectMomZ(steam, FRACUNIT + 2*P_RandomFixed(), true)
						P_InstaThrust(steam, FixedAngle(P_RandomKey(360)*FRACUNIT), 2*P_RandomFixed())
						if (point.info.painsound) then
							S_StartSound(steam, point.info.painsound)
						end
					end
				end
			else
				local distx = P_ReturnThrustX(point, point.angle, point.radius)
				local disty = P_ReturnThrustY(point, point.angle, point.radius)
				if (P_TryMove(point, point.x + distx, point.y + disty, false) -- prevents the sprite from clipping into the wall or dangling off ledges
					and P_TryMove(point, point.x - 2*distx, point.y - 2*disty, false)
					and P_TryMove(point, point.x + distx, point.y + disty, false))
				then
					if (point.info.seesound) then
						S_StartSound(point, point.info.seesound)
					end
				else
					P_RemoveMobj(point)
				end
			end
		end
	end

	if (duration > 1) then
		mo.flags2 =  $ | MF2_FIRING
	else
		mo.flags2 = $ & ~MF2_FIRING
	end
end

local playerdist = 6 --adjust this as needed to change base search distance when looking for a nearby player before shooting

local function pulsateOverlay(mo)

	if mo.state == S_LASERDUDE_STARTUP1 then
		mo.health = mo.health + 1
	elseif mo.state == S_LASERDUDE_STARTUP2 then
		mo.health = mo.health + 2
	elseif mo.state == S_LASERDUDE_STARTUP3 then
		mo.health = 100
	end


	if mo.health == 0 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS90
	elseif mo.health > 1 and mo.health <= 10 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS90
	elseif mo.health > 10 and mo.health <= 20 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS80
	elseif mo.health > 20 and mo.health <= 30 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS70
	elseif mo.health > 30 and mo.health <= 40 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS60
	elseif mo.health > 40 and mo.health <= 50 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS50
	elseif mo.health > 50 and mo.health <= 60 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS40
	elseif mo.health > 60 and mo.health <= 70 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS30
	elseif mo.health > 70 and mo.health <= 80 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS20
	elseif mo.health > 80 and mo.health <= 90 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD|FF_TRANS10
	elseif mo.health > 90 then
		mo.overlay.frame = FF_FULLBRIGHT|FF_ADD
	end
end



local function findBouncerTarget(mo, mp)
	
	if mp.args[0] == 1 then
		for mt in mapthings.tagged(mp.args[1])
			if mt.type == 3006 then
				mo.bouncerTarget = mt.mobj
				--print("Found ya bitch!")
				return
			end
		end
	end
end




addHook("MobjThinker", function(mo)
	
	if mo.valid and mo then
		pulsateOverlay(mo)
	end
	
	if mo.state == S_LASERDUDE_LASER3 and mo.target == nil then
		if mo.spawnpoint and mo.spawnpoint.args[0] == 1 and not mo.bouncerTarget then
			findBouncerTarget(mo, mo.spawnpoint)
			return
		else
			A_FindTarget(mo, MT_LASERTARGET, 0)
	end 
		end
			
	mo.lasertime = $ or false
	
	if mo.state == S_LASERDUDE_LASER4 and mo.target ~= nil then --ready to shoot and have a target orb?
		for p in players.iterate do --lets look for players first
			if not (multiplayer and p.playerstate ~= PST_LIVE) 
			and not (R_PointToDist2(mo.x, mo.y, p.mo.x, p.mo.y) > RING_DIST * playerdist) then --if we're in mp and our prospected player is ded
				--are we close enough? if not then stop it
				--A_Boss1Laser(mo, MT_LASER, (0*65536)+500) --still there? then shoot
				mo.extravalue2 = 60
				mo.lasertime = true
				return --end the search so we don't create multiple lasers per player
			end
		end
	end
	if mo.lasertime then
		if mo.extravalue2 > 0 and mo.target then
			ContinuousLaser(mo, MT_LASER, 4)
		else
			mo.lasertime = false
			mo.extravalue2 = 0
		end
	end
	--print(mo.target and mo.target.x or "searching")
end, MT_LASERDUDE)


addHook("MobjSpawn", function(mo)
	mo.overlay = P_SpawnMobjFromMobj(mo, mo.x, mo.y, mo.z, MT_OVERLAY)
	mo.overlay.state = S_INVISIBLE
	mo.overlay.target = mo
	mo.overlay.sprite = SPR_OBEM
	mo.overlay.dispoffset = 1
	S_StartSound(mo, sfx_bstrt)
end, MT_LASERDUDE)



addHook("MobjSpawn", function(mo)
	mo.overlay = P_SpawnMobjFromMobj(mo, mo.x, mo.y, mo.z, MT_OVERLAY)
	mo.overlay.state = S_INVISIBLE
	mo.overlay.target = mo
	mo.overlay.sprite = SPR_OBAL
	mo.overlay.frame = FF_FULLBRIGHT|FF_ADD
	mo.overlay.dispoffset = 1
	
end, MT_LASERTARGET)

addHook("MobjSpawn", function(mo)
	if mapheaderinfo[gamemap].lvlttl == "Sunken Plant"
	or mapheaderinfo[gamemap].lvlttl == "Nitric Citadel" then
		mo.colorized = true
		mo.color = SKINCOLOR_LIME
		mo.renderflags = RF_FULLBRIGHT
	end
end, MT_EGGMOBILE_FIRE)


addHook("ShouldDamage", function(mo, moAttacker, moDaddy, type)
	
	if moDaddy.type == MT_LASERDUDE then
		return false
	end


end, MT_LASERTARGET)
