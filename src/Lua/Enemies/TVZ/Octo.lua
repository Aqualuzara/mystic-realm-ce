freeslot(
"MT_OCTOOP",
"S_OCTOOP",
"MT_OCTOOP_TENTACLE_PURPLE",
"S_OCTOOP_TENTACLE_PURPLE",
"MT_OCTOOP_TENTACLE_YELLOW",
"S_OCTOOP_TENTACLE_YELLOW",
"MT_OCTOOP_TENTACLE_END",
"S_OCTOOP_TENTACLE_END",
"MT_OCTOOP_PROJECTILE",
"S_OCTOOP_PROJECTILE",
"SPR_OCTO",
"SPR_TNCL"
)



mobjinfo[MT_OCTOOP] = {
--$Category Tempest Valley
--$Name Octopump (INCOMPLETE)
--$Sprite OCTOA1
	doomednum = 3208,
	spawnstate = S_OCTOOP,
	spawnhealth = 1,
	deathstate = S_XPLD_FLICKY,
	deathsound = sfx_pop,
	radius = 24*FRACUNIT,
	height = 72*FRACUNIT,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY
}

states[S_OCTOOP] = {
	sprite = SPR_OCTO,
	frame = A,
	tics = -1,
	nextstate = S_OCTOOP
}

mobjinfo[MT_OCTOOP_TENTACLE_PURPLE] = {
	doomednum = -1,
	spawnstate = S_OCTOOP_TENTACLE_PURPLE,
	spawnhealth = 1,
	deathstate = S_XPLD1,
	radius = 4*FRACUNIT,
	height = 4*FRACUNIT,
	dispoffset = -2,
	flags = MF_NOBLOCKMAP|MF_NOGRAVITY
}

states[S_OCTOOP_TENTACLE_PURPLE] = {
	sprite = SPR_TNCL,
	frame = A,
	tics = -1,
	nextstate = S_OCTOOP_TENTACLE_PURPLE
}

mobjinfo[MT_OCTOOP_TENTACLE_YELLOW] = {
	doomednum = -1,
	spawnstate = S_OCTOOP_TENTACLE_YELLOW,
	spawnhealth = 1,
	deathstate = S_XPLD1,
	radius = 4*FRACUNIT,
	height = 4*FRACUNIT,
	dispoffset = -2,
	flags = MF_NOBLOCKMAP|MF_NOGRAVITY
}

states[S_OCTOOP_TENTACLE_YELLOW] = {
	sprite = SPR_TNCL,
	frame = B,
	tics = -1,
	nextstate = S_OCTOOP_TENTACLE_YELLOW
}

mobjinfo[MT_OCTOOP_TENTACLE_END] = {
	doomednum = -1,
	spawnstate = S_OCTOOP_TENTACLE_END,
	spawnhealth = 1,
	deathstate = S_XPLD1,
	radius = 4*FRACUNIT,
	height = 12*FRACUNIT,
	dispoffset = -2,
	flags = MF_NOBLOCKMAP|MF_NOGRAVITY
}

states[S_OCTOOP_TENTACLE_END] = {
	sprite = SPR_TNCL,
	frame = C,
	tics = -1,
	nextstate = S_OCTOOP_TENTACLE_END
}

mobjinfo[MT_OCTOOP_PROJECTILE] = {
	doomednum = -1,
	spawnstate = S_OCTOOP_PROJECTILE,
	spawnhealth = 1,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	flags = MF_SPECIAL|MF_MISSILE
}

states[S_OCTOOP_PROJECTILE] = {
	sprite = SPR_CANG,
	frame = A|TR_TRANS20,
	tics = -1,
	nextstate = S_OCTOOP_PROJECTILE
}






local firinginterval = 50

--mo.tentacles[mo.tentaclepurple1[1]]
--mo.tentacles[mo.tentacleyellow[1]]
--mo.tentacles[mo.tentaclepurple2[1]]
--mo.tentacles[mo.tentacleend[1]]

addHook("MobjSpawn", function(mo)
	mo.tentaclepurple1 = {}
	mo.tentacleyellow = {}
	mo.tentaclepurple2 = {}
	mo.tentacleend = {}

	mo.anglecounter = ANG1
	mo.firingcounter = firinginterval
	mo.fired = 0
	for i=1,4 do
		mo.tentaclepurple1[i] = P_SpawnMobj(
			mo.x,
			mo.y,
			mo.z,
			MT_OCTOOP_ALT_TENTACLE_PURPLE
		)

		mo.tentacleyellow[i] = P_SpawnMobj(
			mo.x,
			mo.y,
			mo.z,
			MT_OCTOOP_ALT_TENTACLE_YELLOW
		)

		mo.tentaclepurple2[i] = P_SpawnMobj(
			mo.x,
			mo.y,
			mo.z,
			MT_OCTOOP_ALT_TENTACLE_PURPLE
		)

		mo.tentacleend[i] = P_SpawnMobj(
			mo.x,
			mo.y,
			mo.z,
			MT_OCTOOP_ALT_TENTACLE_END
		)

		mo.tentaclepurple1[i].target = mo
		mo.tentacleyellow[i].target = mo
		mo.tentaclepurple2[i].target = mo
		mo.tentacleend[i].target = mo
	end
end, MT_OCTOOP)

addHook("MobjThinker", function(mo)
	mo.anglecounter = $ + (ANG1*2)

	if P_LookForPlayers(mo, 2048*FRACUNIT) then
		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)

		if mo.fired == 0 then
			mo.firingcounter = $ - 1
		end

		if mo.firingcounter == 0 then
			mo.projectile = P_SpawnMobj(
				mo.x,
				mo.y,
				mo.z,
				MT_OCTOOP_FIREDGOOP
			)
			mo.projectile.target = mo
			mo.projectile.scale = $ / 2
			mo.projectile.tracer = mo.target

			local hdist = R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y)

			mo.projectile.momx = FixedMul(FRACUNIT+(hdist/30), cos(mo.angle))
			mo.projectile.momy = FixedMul(FRACUNIT+(hdist/30), sin(mo.angle))

			S_StartSound(mo, sfx_gspray)

			mo.fired = 1
		end
	end

	if mo.fired == 1 then
		mo.firingcounter = firinginterval
		mo.fired = 0
	end

	for i=1,4 do
		if mo.valid
		and mo.tentaclepurple1[i].valid
		and mo.tentacleyellow[i].valid
		and mo.tentaclepurple2[i].valid
		and mo.tentacleend[i].valid then
			P_MoveOrigin(
				mo.tentaclepurple1[i],
				mo.x+FixedMul(20*FRACUNIT, cos((ANGLE_90*i)+mo.anglecounter)),
				mo.y+FixedMul(20*FRACUNIT, sin((ANGLE_90*i)+mo.anglecounter)),
				mo.z+FixedMul(5*FRACUNIT, sin(-mo.anglecounter))
			)

			P_MoveOrigin(
				mo.tentacleyellow[i],
				mo.x+FixedMul(40*FRACUNIT, cos((ANGLE_90*i)+mo.anglecounter)),
				mo.y+FixedMul(40*FRACUNIT, sin((ANGLE_90*i)+mo.anglecounter)),
				mo.z+FixedMul(25*FRACUNIT, sin(-mo.anglecounter))
			)

			P_MoveOrigin(
				mo.tentaclepurple2[i],
				mo.x+FixedMul(60*FRACUNIT, cos((ANGLE_90*i)+mo.anglecounter)),
				mo.y+FixedMul(60*FRACUNIT, sin((ANGLE_90*i)+mo.anglecounter)),
				mo.z+FixedMul(55*FRACUNIT, sin(-mo.anglecounter))
			)

			P_MoveOrigin(
				mo.tentacleend[i],
				mo.x+FixedMul(70*FRACUNIT, cos((ANGLE_90*i)+mo.anglecounter)),
				mo.y+FixedMul(70*FRACUNIT, sin((ANGLE_90*i)+mo.anglecounter)),
				mo.z+FixedMul(65*FRACUNIT, sin(-mo.anglecounter))
			)
		end
	end

	mo.z = $ + sin(mo.anglecounter)
end, MT_OCTOOP_ALT)

addHook("MobjDeath", function(mo)
	for i=1,4 do
		P_KillMobj(mo.tentaclepurple1[i])
		P_KillMobj(mo.tentacleyellow[i])
		P_KillMobj(mo.tentaclepurple2[i])
		P_KillMobj(mo.tentacleend[i])
	end
end, MT_OCTOOP)

addHook("MobjThinker", function(mo)
	mo.rollangle = $ + (ANGLE_45)
end, MT_OCTOOP_FIREDGOOP)