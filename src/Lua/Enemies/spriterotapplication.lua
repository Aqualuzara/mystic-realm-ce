local function distrot(mo)
	if mo.target or mo.tracer then
		MRCElibs.slopeRotation(mo)
	end
end

for _,v in ipairs({
	MT_BLUECRAWLA,
	MT_REDCRAWLA,
	MT_ROBOHOOD,
	MT_EGGGUARD,
	MT_EGGSHIELD,
	MT_PENGUINATOR,
}) do
	addHook("MobjThinker", distrot, v)
end

local function fishrot(mo)
	if mo.target or mo.tracer then
		local dist = R_PointToDist2(0, 0, mo.momx, mo.momy)
		local pitch = (R_PointToAngle2(0, 0, mo.momx, mo.momy) - mo.angle)/8
		local roll = R_PointToAngle2(0, 0, dist, mo.momz)
		MRCElibs.cameraSpriteRot(mo, mo.angle, roll, pitch)
	end
end

-- Move this to it's own source file
local function googlecrawlarisedown(mo)
	fishrot(mo)
	if mo.watertop then
		mo.flags = $|MF_NOGRAVITY
		if mo.target then
			if mo.watertop > mo.z and mo.watertop > mo.target.z then
				local hypot = FixedHypot(mo.momx, mo.momy)
				mo.momz = ease.linear(FRACUNIT/24, mo.momz, hypot*TBSlib.sign(mo.target.z-mo.z))
			else
				mo.target = nil
				mo.flags = $ &~ MF_NOGRAVITY
			end
		end
	else
		if P_IsObjectOnGround(mo) then
			local ang = P_RandomRange(1,360)*ANG1
			mo.momx = FixedMul(cos(ang), mo.radius)*2
			mo.momy = FixedMul(sin(ang), mo.radius)*2
			mo.momz = P_MobjFlip(mo)*mo.height
		end
		mo.flags = $ &~ MF_NOGRAVITY
		return true
	end
end

addHook("MobjThinker", googlecrawlarisedown, MT_GOGGLESCRAWLA_SLOW)
addHook("MobjThinker", googlecrawlarisedown, MT_GOGGLESCRAWLA_FAST)

addHook("MobjThinker", function(mo)
	if mo.state < S_BUMBLEBORE_RAISE and mo.state > S_BUMBLEBORE_SPAWN then
		fishrot(mo)
	else
		mo.rollangle = 0
	end
end, MT_BUMBLEBORE)

local function ballrotation(mo)
	if mo.momx or mo.momy or mo.momz then
		mo.pitch = $+FixedAngle(R_PointToDist2(0, 0, R_PointToDist2(0, 0, mo.momx, mo.momy), mo.momz))/8
		MRCElibs.cameraSpriteRot(mo, mo.angle, 0, mo.pitch)
	end
end

addHook("MobjThinker", ballrotation, MT_BIGMINE)

for _,v in ipairs({
	MT_VULTURE,
	MT_DETON,
	MT_JETJAW,
}) do
	addHook("MobjThinker", fishrot, v)
end