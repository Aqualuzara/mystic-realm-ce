--[[
MRCE Lua HUD

(C) 2020-2022 by K. "ashifolfi" J.
]]

local anim_percent
local anim_percent2
local anim_capped
local anim_capped2
local anim_ticker = 0
local anim_ticker2 = 0
local slidein = "no"
local hticker = 0

local TICRATE = TICRATE
local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local FU = FU

local ease_outquart = ease.outquart

local max = max
local min = min

local draw
local drawScaled
local ipairs = ipairs

local G_TicsToCentiseconds = G_TicsToCentiseconds
local G_TicsToMinutes = G_TicsToMinutes
local P_RandomRange = P_RandomRange
local tostring = tostring


if not MarioSkins then
	rawset(_G, "MarioSkins", {})
end

local function HudToggle(player, arg)
    if arg and player and player.mrce then
        if arg == "1" or arg == "on" or arg == "default" or arg == "true" or arg == "normal" or arg == "yes" then
			player.mrce.hud = 1
			if player.mrce.constext == 0 then
				CONS_Printf(player, "MRCE Custom hud enabled")
			end
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
		elseif arg == "off" or arg == "0" or arg == "no" or arg == "false" or arg == "disable" then
			player.mrce.hud = 0
			if player.mrce.constext == 0 then
				CONS_Printf(player, "MRCE Custom hud disabled")
			end
			if io and player == consoleplayer then
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
        end
    elseif player.mrce.constext == 0 then
        CONS_Printf(player, "Toggle MRCE lua hud. Note many effects are used by the custom hud, and are lost when disabled, so BEWARE")
    end
end

COM_AddCommand("mr_hud", function(player, arg)
	HudToggle(player, arg)
end)

local ring_outline = {0,-1,0,1,1,0,-1,0}

-- I am not doing everything.
-- PLEASE JUST AVOID OVERUSING C-FUNCTIONS LIKE v.cachePatch when you don't have to. They run every tic, ya know?
local MRHSCORE, MRHTIME, MRHRINGS, MRHCOINS, MRHRRING, MRHRCOIN, MRHPRING, SUBTRACKBG
-- not literal globals, just variables for global hud use.
-- While yes 27*FRACUNIT thousand times won't cause performance issues
-- Just please, these constantly have to be calculated
local HUD_GLOBAL_XI = -300
local HUD_GLOBAL_X = HUD_GLOBAL_XI*FU
local HUD_GLOBAL_TWZ = 26*FRACUNIT
-- Same with flags, bit-wise operations are still instructions
local TOP_LEFT_FLAGSBASE = V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER
local TOP_LEFT_FLAGS = 0
-- Not gonna lie, there could be made a lot of optimalizations, pretty sure font drawer is also like that.

local TIC_FRAC = FU / TICRATE

local function pad(str, num, symb)
	local dif = num - str:len()
	local result = str
	if dif > 0 then
		result = string.rep(symb, dif)..result
	end
	return result
end

local lastrings = 0
local rotateringhud = 0
local x_exp_ease = 0
local colormap

local function DrawMRCEHUD(v, p, cam, ticker, endticker)
	if p.spectator then return end
	if not p.realmo then return end

	if p.exiting <= 50
	and p.exiting > 0 then
		if anim_ticker2 > 44 then
			anim_ticker2 = 44
		end
		anim_ticker2 = $ - 1
	end

	if not MRHSCORE then
		MRHRINGS = v.cachePatch("RMRCEHUDTT")
		MRHCOINS = v.cachePatch("MRHCOINS")
		MRHRRING = v.cachePatch("MRHRRING")
		MRHRCOIN = v.cachePatch("MRHRCOIN")
		MRHSCORE = v.cachePatch("SMRCEHUDTT")
		MRHTIME = v.cachePatch("TMRCEHUDTT")

		-- purple
		MRHPRING = v.cachePatch("MRHPRING")

		SUBTRACKBG = v.cachePatch("SUBMRCEBG")

		draw = v.draw
		drawScaled = v.drawScaled
	end

	TOP_LEFT_FLAGS = TOP_LEFT_FLAGSBASE|(v.userTransFlag())
	--if mapheaderinfo[gamemap].mysticrealms ~= 'true' then
		TOP_LEFT_FLAGS = $|V_HUDTRANS
	--end


	anim_capped = max(min(anim_ticker, TICRATE), 0)
	anim_percent = TIC_FRAC * anim_capped

	anim_capped2 = max(min(anim_ticker2, TICRATE), 0)
	anim_percent2 = TIC_FRAC * anim_capped2

	-- Colormap is also C call having to be made.
	colormap = v.getColormap(p.realmo.skin, p.realmo.color)
	x_exp_ease = ease_outquart(anim_percent, -300 , 20)
end
hud.add(DrawMRCEHUD, "game")

customhud.SetupItem("score", "mrce", function(v, p, t, e)
	if not MRHSCORE and not ((p.mo.skin == "samus") or (p.mo.skin == "basesamus"))
	and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) then return end
	if mapheaderinfo[gamemap].mrce_emeraldstage then
		return
	end

	-- Score
	v.draw(x_exp_ease-4, 28, MRHSCORE, TOP_LEFT_FLAGS, colormap)
	TBSlib.fontdrawerInt(v, "MRCEHUDFNT", x_exp_ease+39, 27, p.score, TOP_LEFT_FLAGS, nil, "left", -1, 8, '0')
end, "game")

customhud.SetupItem("time", "mrce", function(v, p, t, e)
	if not MRHSCORE and not ((p.mo.skin == "samus") or (p.mo.skin == "basesamus"))
	and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) then return end
	if mapheaderinfo[gamemap].mrce_emeraldstage then
		return
	end

		local minnutevalue = "00"
		local secondsvalue = "00"
		local centisevalue = "00"

		-- Time
		v.draw(x_exp_ease+7, 10, MRHTIME, TOP_LEFT_FLAGS, colormap)
		if G_TicsToMinutes(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			minnutevalue = '0'..G_TicsToMinutes(p.realtime)
		elseif G_TicsToMinutes(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			minnutevalue = G_TicsToMinutes(p.realtime)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			minnutevalue = tostring(p.warpminutes)
		end

		if G_TicsToSeconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			secondsvalue = '0'..G_TicsToSeconds(p.realtime)
		elseif G_TicsToSeconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			secondsvalue = G_TicsToSeconds(p.realtime)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			secondsvalue = tostring(p.warpseconds)
		end

		if G_TicsToCentiseconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			centisevalue = '0'..G_TicsToCentiseconds(p.realtime)
		elseif G_TicsToCentiseconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" then
			centisevalue = G_TicsToCentiseconds(p.realtime)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then
			centisevalue = tostring(p.warpcentiseconds)
		end

		TBSlib.fontdrawerInt(v, "MRCEHUDFNT", ease_outquart(anim_percent, HUD_GLOBAL_XI, 67), 9, minnutevalue..":"..secondsvalue.."."..centisevalue, TOP_LEFT_FLAGS, nil, "left", -1)
end, "game")

customhud.SetupItem("rings", "mrce", function(v, p, t, e)
	if not MRHSCORE and not ((p.mo.skin == "samus") or (p.mo.skin == "basesamus"))
	and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) then return end
	if mapheaderinfo[gamemap].mrce_emeraldstage then
		return
	end

	v.draw(x_exp_ease-10, 11, SUBTRACKBG, TOP_LEFT_FLAGS|V_REVERSESUBTRACT, colormap)

	-- Rings
	local getrings = p.mo.skin == "xian" and p.xian.rings or p.rings

	if lastrings ~= getrings then
		if getrings > lastrings then
			rotateringhud = $+abs(lastrings - getrings)*24
		end
		lastrings = getrings
	end

	local ringsfont = getrings > 0 and 'MRCEHUDFNT' or ((leveltime % 12)/6 and 'MRCEHURFNT' or 'MRCEHUDFNT')
	local ringcolor = p.mo.pw_combine and v.getColormap(TC_DEFAULT, 0, 'PurpleRingTranslation') or nil

	rotateringhud = max(ease.linear(FRACUNIT/12, rotateringhud, 0), 0)

	if not ultimatemode
	or mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
	and not MarioSkins[p.mo.skin] and not mariomode then --normal hud draw for normal characters
		local x = ease_outquart(anim_percent2, -300 , 17)
		local ringtimer = (rotateringhud) % 23
		local ring = v.getSpritePatch(SPR_RING, ringtimer)
		local flip = (ringtimer/12) and V_FLIP or 0
		local color = v.getColormap(TC_ALLWHITE, 31)
		local xwidth = ring.width/4
		local xheight = ring.height/2
		v.draw(ease_outquart(anim_percent2, -300 , 13), 28, MRHRINGS, TOP_LEFT_FLAGS, colormap)
		for i = 2, 8, 2 do
			local xoff = ring_outline[i-1]
			local yoff = ring_outline[i]
			v.drawScaled((x+xoff+xwidth) << FRACBITS, (26+yoff+xheight) << FRACBITS, FRACUNIT/2, ring, TOP_LEFT_FLAGS|flip, color)
		end
		v.drawScaled((x+xwidth) << FRACBITS, (26+xheight) << FRACBITS, FRACUNIT/2, ring, TOP_LEFT_FLAGS|flip, ringcolor)
		TBSlib.fontdrawerInt(v, ringsfont, ease_outquart(anim_percent2, HUD_GLOBAL_XI, 50), 32, tostring(getrings), TOP_LEFT_FLAGS, nil, "right", -1, 3, '0')
	elseif not ultimatemode
	or mapheaderinfo[gamemap].lvlttl == "Dimension Warp" then  --mario time, oh yeah! I'd like to swap the rings graphic out for a coins version where appropriate, just need a graphic for it
		v.draw(ease_outquart(anim_percent2, HUD_GLOBAL_XI, 13), 28, MRHCOINS, TOP_LEFT_FLAGS, colormap)
		TBSlib.fontdrawerInt(v, "MRCEHUDFNT", ease_outquart(anim_percent2, HUD_GLOBAL_XI, 50), 32, tostring(p.rings), TOP_LEFT_FLAGS, nil, "right", -1, 3, '0')
	end
end, "game")

addHook("MapLoad", function()
  anim_ticker = 0
  anim_ticker2 = 0
end)

hud.add(function(v,p,ticker,endticker)
	if p.exiting then return end
	if modeattacking then return end
	if ticker == 1 then
		anim_ticker = 0
		anim_ticker2 = 0
	end
	if ticker > 75 then
		anim_ticker = $ + 1
		anim_ticker2 = $ + 1
	end
end, "titlecard")

addHook("ThinkFrame", function()
	if displayplayer and displayplayer.exiting then return end
	if not modeattacking then return end
	if leveltime == 1 then
		anim_ticker = 0
		anim_ticker2 = 0
	elseif leveltime > 35 and leveltime < 80 then
		anim_ticker = $ + 1
		anim_ticker2 = $ + 1
	end
end)

addHook("PlayerThink", function(p)
	if (mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp") then return end
	if not (leveltime%35)
	or (leveltime%TICRATE == TICRATE/5) or (leveltime%TICRATE == ((TICRATE/5) * 3)) or (leveltime%TICRATE == ((TICRATE/5) * 2)) or (leveltime%TICRATE == ((TICRATE/5) * 4)) then
		p.warpminutes =  P_RandomRange(10, 99)
		p.warpseconds =  P_RandomRange(10, 99)
		p.warpcentiseconds =  P_RandomRange(10, 99)
	end
end)