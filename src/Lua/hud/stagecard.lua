local Delay = TICRATE/2+TICRATE/5
local Start = 1 + Delay
local End = 62 + Delay

--local Easing_In = 6 + Delay
local Easing_Out = 52 + Delay

customhud.SetupItem("stagetitle", "mrce", function(v, p, t, e)
	if t > End or t < Start then return end

	local timer_in = min(t - Start, 5)*FRACUNIT/5
	local timer_out = max(min(t - Easing_Out, 10), 0)*FRACUNIT/10

	local offset = ease.linear(timer_in - timer_out, 250, 0)


	local header = mapheaderinfo[gamemap]

	local real_t = t - Delay

	if header.mrce_emeraldstage then
		return
	end

	v.draw(0, 0, v.cachePatch("MRCETTBASE"..real_t), V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	local text = header.lvlttl
	local width = v.levelTitleWidth(text)
	local height = v.levelTitleHeight(text)

	v.drawLevelTitle(231 - width + offset, 136 - height, text, V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	v.drawScaledNameTag((262 + offset)*FRACUNIT, 130*FRACUNIT, header.actnum, V_SNAPTORIGHT|V_SNAPTOBOTTOM, 3*FRACUNIT/2, SKINCOLOR_GOLD, SKINCOLOR_BLACK)

	v.drawString(228 + offset, 144, header.subttl, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "right")
end, "titlecard")