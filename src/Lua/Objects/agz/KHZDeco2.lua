--Used slots

freeslot("MT_OAKFALLSLOTUS", "MT_OAKFALLSLILY", "S_OAKFALLSLILYNEW", "S_OAKFALLSLILY", "S_OAKFALLSLOTUS", "SPR_0KHX")

-- Lily pad section

-- FOR SOME REASON THIS DOESN'T WORK IN LUA? what the hell, moved to agzdecoration.soc. Seriously, I have no interest fighting poor
-- internal mobjinfo metatable.

--[[
mobjinfo[MT_OAKFALLSLOTUS] = {
	dommednum = -1,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 1,
	spawnstate = S_OAKFALLSLOTUS,
	deathstate = S_INVISIBLE,
	seestate = S_INVISIBLE,
	radius = 8*FRACUNIT,
	height = 16*FRACUNIT,
	deathsound = sfx_pop,
	mass = 100,
	flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY,
}
]]

states[S_OAKFALLSLOTUS] = {
	sprite = SPR_0KHX,
	frame = B,
}

--[[
mobjinfo[MT_OAKFALLSLILY] = {
--$Category SRB1 KHZ Vegetation
--$Name KHZ Lily pad
--$Sprite 0KHXA0
--$FlatSprite
	dommednum = 830,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 1,
	spawnstate = S_OAKFALLSLILYNEW,
	deathstate = S_INVISIBLE,
	seestate = S_INVISIBLE,
	radius = 8*FRACUNIT,
	height = 16*FRACUNIT,
	deathsound = sfx_pop,
	mass = 100,
	flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY,
}
]]

-- Water Lylie states

local xcolors = {
	SKINCOLOR_ORANGE,
	SKINCOLOR_RED,
	SKINCOLOR_CHERRY,
	SKINCOLOR_SIBERITE,
	SKINCOLOR_BLUEBELL,
	SKINCOLOR_SILVER,
}

states[S_OAKFALLSLILYNEW] = {
	sprite = SPR_0KHX,
	frame = A|FF_FLOORSPRITE,
	tics = 1,
	nextstate = S_OAKFALLSLILY,
}

addHook("MapThingSpawn", function(a, mt)
	a.renderflags = $|RF_NOSPLATBILLBOARD
	if not (#mt % 3) then
		local angle = mt.angle*ANG1
		local lotus = P_SpawnMobjFromMobj(a, -24*cos(angle), -24*sin(angle), 0, MT_OAKFALLSLOTUS)
		lotus.color = xcolors[P_RandomKey(#xcolors) + 1]
		lotus.scale = $+min((a.scale/3), FRACUNIT/4)
	end
end, MT_OAKFALLSLILY)

states[S_OAKFALLSLILY] = {
	sprite = SPR_0KHX,
	frame = A|FF_FLOORSPRITE,
	action = function(a)
		a.renderflags = $|RF_NOSPLATBILLBOARD
	end,
}
