freeslot(
	"MT_FAKEEMERALD1"
)

local tradestart = false
-- WE NEED TO MOVE FREESLOTS INTO LUA, WTH
-- THIS DOESN'T NEEDS TO BE IN THERE
local color_table = {
	[S_FAKEEMERALD1] = SKINCOLOR_SAPPHIRE,
	[S_FAKEEMERALD2] = SKINCOLOR_LAVENDER,
	[S_FAKEEMERALD3] = SKINCOLOR_APPLE,
	[S_FAKEEMERALD4] = SKINCOLOR_SALMON,
	[S_FAKEEMERALD5] = SKINCOLOR_ARCTIC,
	[S_FAKEEMERALD6] = SKINCOLOR_SUNSET,
	[S_FAKEEMERALD7] = SKINCOLOR_BONE,

	[S_FAKEEMERALD8] = SKINCOLOR_GREY,
}

addHook("MobjThinker", function(mobj)
    if not (mobj and mobj.valid) then return end
	if not (leveltime%35) then
		if mobj.state == S_FAKEEMERALD1 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 83)
		elseif mobj.state == S_FAKEEMERALD2 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 89)
		elseif mobj.state == S_FAKEEMERALD3 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 66)
		elseif mobj.state == S_FAKEEMERALD4 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 36)
		elseif mobj.state == S_FAKEEMERALD5 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 76)
		elseif mobj.state == S_FAKEEMERALD6 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 56)
		elseif mobj.state == S_FAKEEMERALD7 then
			MRCE_superSpark(mobj, 1, 9, 1, 21845, false, 11)
		end
	end
	if mobj.state >= S_FAKEEMERALD1 and mobj.state <= S_FAKEEMERALD8 then
		if not mobj.overlay then
			mobj.frame = $|FF_TRANS20
			mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
			mobj.overlay.state = S_INVISIBLE
			mobj.overlay.sprite = SPR_OILF
			mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
			mobj.overlay.color = color_table[mobj.state] or SKINCOLOR_APPLE
			mobj.overlay.colorized = true
			mobj.overlay.target = mobj

			local scale = FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = 32*FRACUNIT
		else
			local scale = min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		end
	end

	for i = 1, 7, 1 do
		if mobj and mobj.valid and(mobj.state == _G["S_FAKEEMERALD" .. i] and not (emeralds and _G["EMERALD" .. i])) then
			P_RemoveMobj(mobj)
		end
	end

	--[[
	if ((emeralds & EMERALD1) ~= 1 and mobj.state == S_FAKEEMERALD1
	or (emeralds & EMERALD2) ~= 2 and mobj.state == S_FAKEEMERALD2
	or (emeralds & EMERALD3) ~= 4 and mobj.state == S_FAKEEMERALD3
	or (emeralds & EMERALD4) ~= 8 and mobj.state == S_FAKEEMERALD4
	or (emeralds & EMERALD5) ~= 16 and mobj.state == S_FAKEEMERALD5
	or (emeralds & EMERALD6) ~= 32 and mobj.state == S_FAKEEMERALD6
	or (emeralds & EMERALD7) ~= 64 and mobj.state == S_FAKEEMERALD7)
	and mobj.valid then
		P_RemoveMobj(mobj)
	end]]
	if gamemap == 121 then
		if mobj.portalslide and mrce.agz_portalopener and mrce.agz_portalopener >= 7 then
			mobj.flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
			if mobj.state == S_PORTALSCRAP1 then
				P_InstaThrust(mobj, ANGLE_90, 70*FRACUNIT)
				mobj.rollangle = $ + ANG2
				mobj.angle = ANGLE_90
			else
				P_InstaThrust(mobj, ANGLE_90, 80*FRACUNIT)
				mobj.rollangle = $ + ANG10
			end
			if mobj.y > 12709*FRACUNIT and mobj.valid then
				P_KillMobj(mobj)
			end
		end
		if mobj.valid and mobj.portalpull and mrce.agz_portalopener >= 7 then
			mobj.flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
			P_Thrust(mobj, (R_PointToAngle2(mobj.x, mobj.y, 0, 320*FRACUNIT)), FRACUNIT)
			if mobj.x > -768*FRACUNIT
			and mobj.y > 256*FRACUNIT
			and mobj.x < 768*FRACUNIT
			and mobj.y < 384*FRACUNIT then
				P_RemoveMobj(mobj)
			end
		end
	end

	if mobj and mobj.valid and mobj.target and mobj.target.player then
		if mapheaderinfo[gamemap].lvlttl == "Inner Sanctum" then
			local a = ANG1+5400000
			mobj.target.emeraldexchangenownineninetynine = $ + 1
			if mobj.target.emeraldexchangenownineninetynine > 25*TICRATE then
				a = $*3
			elseif mobj.target.emeraldexchangenownineninetynine > 18*TICRATE then
				a = 0
			end
			mobj.angle = $+a
			mobj.scale = mobj.target.scale
			local xyr = 1*FU
			local zr = 1*FU
			if mobj.target.emeraldexchangenownineninetynine < 18*TICRATE then
				xyr = min(48*FU, (mobj.target.emeraldexchangenownineninetynine*FU)/2)
				zr = min(8*FU, (mobj.target.emeraldexchangenownineninetynine*FU)/4)
			elseif mobj.target.emeraldexchangenownineninetynine > 25*TICRATE then
				xyr = $ + ((FU*mobj.target.emeraldexchangenownineninetynine)/12) --figure out easing another time gotta get this working for now
			else
				xyr = 48*FU
				zr = 8*FU
			end
			local zb = mobj.target.emeraldexchangenownineninetynine > 25*TICRATE and max((mobj.target.emeraldexchangenownineninetynine - (25*TICRATE))*2*FU, 0) or 0
			P_MoveOrigin(mobj, mobj.target.x+FixedMul(xyr, cos(mobj.angle)),
				mobj.target.y+FixedMul(xyr, sin(mobj.angle)),
				(mobj.target.z+(mobj.target.height/2)+FixedMul(zr, cos(mobj.angle-mobj.target.angle+ANG60))) + zb)
			if mobj.target.emeraldexchangenownineninetynine > 32*TICRATE and not tradestart then
				for i = 1, 7, 1 do
					if (emeralds & _G["EMERALD" .. i]) then
						P_LinedefExecute(300 + i, mobj.target)
					end
				end
				tradestart = true
			end
		elseif mobj.emeraldchase then
			local angle = leveltime*ANG1
			local target = mobj.target
			local radius = FixedMul(target.radius+mobj.radius, target.scale)
			local x = target.x + FixedMul(radius, cos(angle)) - target.momx
			local y = target.y + FixedMul(radius, sin(angle)) - target.momy

			MRCElibs.lerpMoveObjectToPos(mobj, FRACUNIT/4, x, y, 16*target.scale+target.z)
		end
	end
	if mobj and mobj.valid and mobj.spawnpoint and mobj.spawnpoint.args[0] then
		mobj.state = (S_FAKEEMERALD1 - 1) + mobj.spawnpoint.args[0]
		if mobj.spawnpoint.args[1] then
			if mobj.spawnpoint.args[1] == 2 then
				mobj.frame = J + (mobj.state - S_FAKEEMERALD1)
			elseif mobj.spawnpoint.args[1] == 3 then
				mobj.frame = R + (mobj.state - S_FAKEEMERALD1)
			end --funny laziness leads to strange optimizations
		end
		--mobj.overlay.color = color_table[mobj.spawnpoint.args[1]]
	end
end, MT_FAKEEMERALD1)

addHook("MobjThinker", function(mobj)
    if not (mobj and mobj.valid) then return end
	if gamemap == 121 then
		if mobj.portalslide and mrce.agz_portalopener and mrce.agz_portalopener >= 7 then
			mobj.flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
			P_InstaThrust(mobj, ANGLE_90, 80*FRACUNIT)
			mobj.rollangle = $ + ANG10
			if mobj.y > 12709*FRACUNIT and mobj.valid then
				P_KillMobj(mobj)
			end
		end
	end
end, MT_SPECCY)

addHook("NetVars", function(net)
	tradestart = net($)
end)