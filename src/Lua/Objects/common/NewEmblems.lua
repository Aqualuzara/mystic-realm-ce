addHook("MobjSpawn", function(emblem)
	emblem.origz = emblem.z
end, MT_EMBLEM)

addHook("MobjThinker", function(emblem)
	if not (emblem and emblem.valid) then return end
	if not (emblem.frame & FF_PAPERSPRITE) then
		emblem.frame = $|FF_PAPERSPRITE
	end
	emblem.angle = $ + FixedAngle(FRACUNIT)
	emblem.z = emblem.origz + 8 * abs(sin(FixedAngle(leveltime*4*FRACUNIT)))
	local emblemparticle = P_SpawnMobjFromMobj(emblem, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK)
	emblemparticle.momx = P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.momy = P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.momz =  P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.colorized = true
	emblemparticle.color = emblem.color
	emblemparticle.fuse = 10
	emblemparticle.scale = emblem.scale *1/6
	emblemparticle.source = emblem
	if (emblem.frame & FF_TRANSMASK) then
		emblemparticle.flags2 = MF2_DONTDRAW
	end
end, MT_EMBLEM)