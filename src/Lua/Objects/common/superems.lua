states[freeslot('S_SUPEREMERALD')] = {
	sprite = freeslot('SPR_0SUP'),
	frame = A|FF_PAPERSPRITE|FF_TRANS20,
}

mobjinfo[freeslot('MT_SUPEREMERALD')] = {
	doomednum = 2224,
	spawnstate = S_SUPEREMERALD,
	radius = 43*FRACUNIT,
	height = 65*FRACUNIT - FRACUNIT/2,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SOLID,
	dispoffset = -1,
}

local def_angle = ANGLE_180/3

addHook("MobjThinker", function(mo)
	local angle_offset = def_angle

	if not mo.sides then
		mo.sides = {}
		for i = 1, 5 do
			local side = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_RAY)
			side.state = S_SUPEREMERALD
			side.frame = i|FF_PAPERSPRITE
			side.angle = mo.angle + angle_offset
			side.flags = mo.flags|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING &~ MF_SOLID
			side.flags2 = $|MF2_LINKDRAW
			side.target = mo

			if i == 3 then
				side.frame = $|FF_FLOORSPRITE
				side.dispoffset = 1
			else
				side.dispoffset = -1
			end

			table.insert(mo.sides, side)
		end
	else
		for i = 1, 5 do
			local side = mo.sides[i]
			if not side then
				TBSlib.removeMobjArray(mo.sides)
				mo.sides = {}

				return
			end

			side.angle = mo.angle + angle_offset
			if i == 3 then
				P_MoveOrigin(side, mo.x, mo.y, mo.z+FixedMul(mo.height, mo.scale))
			else
				P_MoveOrigin(side, mo.x, mo.y, mo.z)
			end



			angle_offset = $ + def_angle
		end
	end

	if not mo.overlay then
		mo.frame = $|FF_TRANS20
		mo.overlay = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_OVERLAY)
		mo.overlay.state = S_INVISIBLE
		mo.overlay.sprite = SPR_OILF
		mo.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mo.overlay.color = SKINCOLOR_APPLE
		mo.overlay.colorized = true
		mo.overlay.target = mo

		local scale = mo.scale + (R_PointToDist(mo.x, mo.y) or 1)/3048
		mo.overlay.spritexscale = scale
		mo.overlay.spriteyscale = scale

		mo.overlay.spriteyoffset = 64*FRACUNIT
	else
		local scale = min(mo.scale + (R_PointToDist(mo.x, mo.y) or 1)/3048, 32*FRACUNIT)
		mo.overlay.spritexscale = scale
		mo.overlay.spriteyscale = scale

		mo.overlay.spriteyoffset = min(FixedDiv(64*FRACUNIT, scale*2), 64*FRACUNIT)
	end

end, MT_SUPEREMERALD)

