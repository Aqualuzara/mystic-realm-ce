freeslot("MT_COMBINE_BOX","MT_COMBINE_GOLDBOX","MT_COMBINE_ICON",
"S_COMBINE_BOX","S_COMBINE_GOLDBOX","S_COMBINE_ICON1","S_COMBINE_ICON2",
"SPR_VMCO")

sfxinfo[sfx_cdfm63].caption = "Rings combined"

mobjinfo[MT_COMBINE_BOX] = {
	--$Name Combine Ring
	--$Sprite VMCOA0
	--$Category Monitors
	--$NotAngled
	doomednum = 427,
	spawnstate = S_COMBINE_BOX,
	reactiontime = 8,
	painstate = S_COMBINE_BOX,
	deathstate = S_BOX_POP1,
	deathsound = sfx_pop,
	speed = 1,
	radius = 18*FRACUNIT,
	height = 40*FRACUNIT,
	mass = 100,
	damage = MT_COMBINE_ICON,
	flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR
}

mobjinfo[MT_COMBINE_GOLDBOX] = {
	--$Name Combine Ring (Respawning)
	--$Sprite VMCOB0
	--$Category Monitors (Respawning)
	--$NotAngled
	doomednum = 457,
	spawnstate = S_COMBINE_GOLDBOX,
	reactiontime = 8,
	attacksound = sfx_monton,
	painstate = S_COMBINE_GOLDBOX,
	deathstate = S_GOLDBOX_OFF1,
	deathsound = sfx_pop,
	speed = 1,
	radius = 20*FRACUNIT,
	height = 44*FRACUNIT,
	mass = 100,
	damage = MT_COMBINE_ICON,
	flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR|MF_GRENADEBOUNCE
}

mobjinfo[MT_COMBINE_ICON] = {
	doomednum = -1,
	spawnstate = S_COMBINE_ICON1,
	seesound = sfx_cdfm63,
	reactiontime = 8,
	speed = 2*FRACUNIT,
	radius = 8*FRACUNIT,
	height = 14*FRACUNIT,
	mass = 100,
	damage = 62*FRACUNIT,
	flags = MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIP|MF_BOXICON|MF_SCENERY
}

function A_CombineRing(actor)
	if actor.target and actor.target.player then
		actor.target.pw_combine = 1
		S_StartSound(actor.target,actor.info.seesound)
	end
end

function A_Sparkling(actor,var1)
-- P_RandomRange can suck my ass
	local rad = ((actor.radius/FRACUNIT)/2)*3
	local h8 = actor.height/FRACUNIT
-- Let's go. Action to replace A_BossScream since that can spawn stuff right in
-- front of your FPS viewpoint, which'll mess you up when RPing as Doomguy.
	local h1 = P_RandomRange(rad/8,rad)*FRACUNIT
	local h2 = P_RandomRange(rad/8,rad)*FRACUNIT
	local ang = actor.angle+FixedAngle(P_RandomRange(45,315)*FRACUNIT)
	local x = FixedMul(h1,cos(ang))
	local y = FixedMul(h2,sin(ang))
	local z = FixedDiv(actor.height/2,actor.scale)+(P_RandomRange(-h8/4,h8/4)*FRACUNIT)
	local spk = P_SpawnMobjFromMobj(actor,x,y,z,var1)
end

states[S_COMBINE_BOX] = {SPR_VMCO,A,2,nil,0,0,S_BOX_FLICKER}
states[S_COMBINE_GOLDBOX] = {SPR_VMCO,B,2,A_GoldMonitorSparkle,0,0,S_GOLDBOX_FLICKER}
states[S_COMBINE_ICON1] = {SPR_VMCO,C|FF_ANIMATE,18,nil,3,4,S_COMBINE_ICON2}
states[S_COMBINE_ICON2] = {SPR_VMCO,C,18,A_CombineRing}

addHook("PlayerThink", function(player)
	if not player.mo then return end
	if not player.mo.pw_combine then player.mo.pw_combine = 0 end

	if player.playerstate == PST_DEAD then
		player.mo.pw_combine = 0
	end
end)

addHook("MobjDamage", function(mo,inf,src)
	if mo.pw_combine and mo.player.rings > 0
	and not (mo.player.powers[pw_shield] & SH_NOSTACK or mo.player.powers[pw_shield] & SH_STACK) then
		P_DoPlayerPain(mo.player,src,inf)
		S_StartSound(mo,sfx_shldls)
		P_PlayerRingBurst(mo.player,1)
		return true
	end
end, MT_PLAYER)

local function CombinedRingThinker(mo)
	if mo.target and mo.target.pw_combine and mo.fuse > 7*TICRATE then
		mo.extravalue1 = mo.target.player.rings
		mo.scale, mo.momx, mo.momy, mo.momz = $*2, $*3, $*3, $*4
		mo.target.player.rings = 0
		mo.target.pw_combine = 0
	end
	if mo.extravalue1 and leveltime % 9 == 0 then
		A_Sparkling(mo,MT_SPARK)
	end
end

addHook("MobjThinker", CombinedRingThinker, MT_FLINGRING)
addHook("MobjThinker", CombinedRingThinker, MT_FLINGCOIN)

local function CombinedRingCollect(ring,plmo)
	if ring.extravalue1 and P_CanPickupItem(plmo.player,false) then
		plmo.player.rings = ring.extravalue1 - 1
	end
end

addHook("TouchSpecial", CombinedRingCollect, MT_FLINGRING)
addHook("TouchSpecial", CombinedRingCollect, MT_FLINGCOIN)

local function CombinedRingBurst(mo)
	if mo.state >= S_SPRK1 then return end

	if mo.extravalue1 then
		if mo.extravalue1 > 32 then
			mo.extravalue1 = 32
		end
		local snd = P_SpawnMobj(mo.x,mo.y,mo.z,MT_AXISTRANSFER)
		P_PlayRinglossSound(snd)
		snd.fuse = 2*TICRATE
		for i = 1, mo.extravalue1 do
			local ang = i*FixedAngle((360*FRACUNIT)/min(mo.extravalue1,16))
			local spl = P_SpawnMobj(mo.x+FixedMul(mo.radius/2, cos(ang)),
									mo.y+FixedMul(mo.radius/2, sin(ang)),
									mo.z+mo.height/2,MT_FLINGRING)
			if i > 16 then
				P_InstaThrust(spl,ang,3*FRACUNIT)
				P_SetObjectMomZ(spl,4*FRACUNIT)
			else
				P_InstaThrust(spl,ang,2*FRACUNIT)
				P_SetObjectMomZ(spl,3*FRACUNIT)
			end
			if mo.eflags & MFE_VERTICALFLIP then
				spl.flags2 = MF2_OBJECTFLIP
			end
			spl.fuse = 8*TICRATE
		end
	end
end

addHook("MobjFuse", CombinedRingBurst, MT_FLINGRING)
addHook("MobjFuse", CombinedRingBurst, MT_FLINGCOIN)
