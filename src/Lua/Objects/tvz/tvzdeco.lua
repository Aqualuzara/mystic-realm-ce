freeslot("MT_TVZBUSH1", "S_TVZBUSH1", "SPR_TBSH")

-- Bush mobjinfo

mobjinfo[MT_TVZBUSH1] = {
--$Category Tempest Valley
--$Name Vine Bush (Large)
--$Sprite TBSHA0
--$NotAngled
	doomednum = 1375,
	spawnstate = S_TVZBUSH1,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 30*FRACUNIT,
	height = 15*FRACUNIT,
	mass = 100,
	flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY
}

-- Bush states

states[S_TVZBUSH1] = {
	sprite = SPR_TBSH,
	frame = A,
	tics = -1,
	nextstate = S_TVZBUSH1
}