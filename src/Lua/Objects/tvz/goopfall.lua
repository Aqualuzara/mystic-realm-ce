/*
	Tempest Valley - Goopfall (Purple&Blue)
	by Henry_3230
	
	Most of the script is edited from the SRB2 source code
	Nearby player check code is partially from the Continue Medal code
*/
freeslot(
"SPR_PFAL",
"S_GOOPFALL_SHOOT_PURPLE",
"S_GOOPFALL_PURPLE1",
"S_GOOPFALL_PURPLE2",
"S_GOOPFALL_PURPLE3",
"MT_GOOPFALL",
"MT_GOOPFALL_GOOP_PURPLE",
"SPR_BFAL",
"S_GOOPFALL_SHOOT_BLUE",
"S_GOOPFALL_BLUE1",
"S_GOOPFALL_BLUE2",
"S_GOOPFALL_BLUE3",
"MT_GOOPFALL_GOOP_BLUE")

--Functions
function A_Goopfall(mo)					-- Spawn falling goop
	local goopfall
	local flag=true
	if (40 - mo.fuse) % (2*(mo.scale >> FRACBITS)) > 1 then		-- Prevent overlap
		return
	end
	for p in players.iterate do			-- No goopfall if no player nearby
		if not p.valid or p.bot or p.spectator then continue end
		if not (p.mo and p.mo.valid) then continue end
		if multiplayer and p.playerstate ~= PST_LIVE then continue end
		if P_AproxDistance(mo.x - p.mo.x, mo.y - p.mo.y) < (mo.info.speed) then
			flag = false
			break 
		end
	end
	if flag then return end
	goopfall = P_SpawnMobjFromMobj(mo, 0, 0, -8*FRACUNIT, mo.goop)
-- 	if goopfall.valid then
		goopfall.momz = P_MobjFlip(mo)*-25*FRACUNIT
-- 	end
end

-- Purple Goopfall
states[S_GOOPFALL_SHOOT_PURPLE] = {
	SPR_PFAL,
	G|FF_ANIMATE,
	2,
	A_Goopfall,
	1,
	1,
	S_GOOPFALL_SHOOT_PURPLE
}
states[S_GOOPFALL_PURPLE1] = {
	SPR_PFAL,
	B|FF_TRANS30,
	1,
	A_FallingLavaCheck,
	0,
	0,
	S_GOOPFALL_PURPLE2
}
states[S_GOOPFALL_PURPLE2] = {
	SPR_PFAL,
	B|FF_TRANS30,
	1,
	A_FallingLavaCheck,
	0,
	0,
	S_GOOPFALL_PURPLE1
}
states[S_GOOPFALL_PURPLE3] = {
	SPR_PFAL,
	C|FF_ANIMATE|FF_TRANS10,
	9,
	nil,
	2,
	3,
	S_NULL
}
mobjinfo[MT_GOOPFALL_GOOP_PURPLE] = {
	spawnstate = S_GOOPFALL_PURPLE1,
	seestate = S_NULL,
	spawnhealth = 1000,
	reactiontime = 8,
	deathstate = S_GOOPFALL_PURPLE3,
	radius = 30*FRACUNIT,
	height = 48*FRACUNIT,
	mass = DMG_WATER,
	flags = MF_SPECIAL|MF_NOGRAVITY
}

-- Blue Goopfall
states[S_GOOPFALL_SHOOT_BLUE] = {
	SPR_BFAL,
	F|FF_ANIMATE,
	2,
	A_Goopfall,
	2,
	1,
	S_GOOPFALL_SHOOT_BLUE
}
states[S_GOOPFALL_BLUE1] = {
	SPR_BFAL,
	B|FF_TRANS30,
	1,
	A_FallingLavaCheck,
	0,
	0,
	S_GOOPFALL_BLUE2
}
states[S_GOOPFALL_BLUE2] = {
	SPR_BFAL,
	B|FF_TRANS30,
	1,
	A_FallingLavaCheck,
	0,
	0,
	S_GOOPFALL_BLUE1
}
states[S_GOOPFALL_BLUE3] = {
	SPR_BFAL,
	C|FF_ANIMATE|FF_TRANS10,
	9,
	nil,
	2,
	3,
	S_NULL
}
mobjinfo[MT_GOOPFALL_GOOP_BLUE] = {
	spawnstate = S_GOOPFALL_BLUE1,
	seestate = S_NULL,
	spawnhealth = 1000,
	reactiontime = 8,
	deathstate = S_GOOPFALL_BLUE3,
	radius = 30*FRACUNIT,
	height = 48*FRACUNIT,
	mass = DMG_WATER,
	flags = MF_PAIN|MF_SPECIAL|MF_NOGRAVITY
}
-- All-in-one thing
mobjinfo[MT_GOOPFALL] = {
	--$Category Tempest Valley
	--$Name Goop fall
	--$Sprite PFALH0
	--$Color 13
	--$NotAngled
	--$Arg0 Initial delay
	--$Arg1 Double size?
	--$Arg1Type 11
	--$Arg1Enum noyes
	--$Arg2 Goop Type
	--$Arg2Type 11
	--$Arg2Enum { 0="Purple"; 1="Blue";}
	doomednum = 2227,
	spawnstate = S_LAVAFALL_DORMANT,
	spawnhealth = 1000,
	seesound = sfx_lvfal1,
	reactiontime = 8,
	attacksound = sfx_s3kd5l,
	speed = 3200*FRACUNIT,
	radius = 30*FRACUNIT,
	height = 32*FRACUNIT,
	dispoffset = 1,
	mass = 100,
	flags = MF_NOGRAVITY|MF_SPAWNCEILING
}
--Function
addHook("MapThingSpawn", function(mo,th)			--Goopfalls Setup
	mo.fuse = 30 + th.args[0]			--Arg0 Initial delay
	if th.args[1] then					--Arg1 Double Size?
		mo.scale = 2*$
	end
	if th.args[2] then					--Arg2 Goop type
		mo.goop = MT_GOOPFALL_GOOP_BLUE
		mo.shootstate = S_GOOPFALL_SHOOT_BLUE
	else
		mo.goop = MT_GOOPFALL_GOOP_PURPLE
		mo.shootstate = S_GOOPFALL_SHOOT_PURPLE
	end
	return true
end, MT_GOOPFALL)
addHook("MobjFuse", function(mo)			-- Goopfall Timing 
	if mo.state == S_LAVAFALL_DORMANT then
		mo.fuse = 30
		mo.state = S_LAVAFALL_TELL
		S_StartSound(mo, mo.info.seesound)
	elseif mo.state == S_LAVAFALL_TELL then
		mo.fuse = 40
		P_SetMobjStateNF(mo, mo.shootstate)
		S_StopSound(mo)
		S_StartSound(mo, mo.info.attacksound)
	else
		mo.fuse = 30
		mo.state = S_LAVAFALL_DORMANT
		S_StopSound(mo)
	end
	return true
end, MT_GOOPFALL)
addHook("TouchSpecial", function(mo,pmo)				-- Let goop make player slow
	pmo.player.slowgooptimer = 9*TICRATE
	return true
end, MT_GOOPFALL_GOOP_PURPLE)
addHook("MobjCollide", function(mo,cmo)					-- Make player and some of the other objects flow.
	if cmo.flags&(MF_NOGRAVITY|MF_NOBLOCKMAP|MF_ENEMY) then return false end		-- Don't flow for certain types of objects.
	if cmo.z > (mo.z + mo.height) then return end
	if mo.z - mo.height > (cmo.z + cmo.height) then return end		-- Height check
	if not cmo.goopfalltimer then									-- Make a sound.
		S_StartSound(pmo, sfx_ghit)
	end
	cmo.goopfalltimer = 2											-- To keep the mobj's status, it has to be set to 2.
	return false					-- No collide.
end, MT_GOOPFALL_GOOP_PURPLE)
addHook("MobjThinker", function(mo)	
	if mo.goopfalltimer then
		mo.goopfalltimer = $-1					-- yep i know that it will make delay
		if abs(mo.momz) < 9*FRACUNIT then		-- Flow.
			P_SetObjectMomZ(mo,FRACUNIT/2,true)
		end
	end
end)