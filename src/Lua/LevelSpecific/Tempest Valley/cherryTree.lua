--Sprite by Renato Madman
--Lua by Speed2411

freeslot(
"MT_BIGCHERRYTREE",
"S_BIGCHERRYTREE",
"SPR_2TVZ")

mobjinfo[MT_BIGCHERRYTREE] = {
--$Category Tempest Valley
--$Name Big Cherry Tree
--$Sprite 2TVZA0
	doomednum = 70,
	spawnstate = S_BIGCHERRYTREE,
	spawnhealth = 1000,
	reactiontime = 8,
	radius = 100*FRACUNIT,
	height = 600*FRACUNIT,
	mass = 16,
	dispoffset = 1,
	flags = MF_SOLID|MF_SCENERY|MF_NOTHINK
}
states[S_BIGCHERRYTREE] = {
	sprite = SPR_2TVZ,
	frame = A
}