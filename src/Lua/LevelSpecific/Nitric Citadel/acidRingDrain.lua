addHook("PlayerSpawn", function(p)
	p.toxicrate = FRACUNIT
	p.timetoacid = 5
	p.acidpower = 1
end)

local function drainScore(p)
	if mrce_global_points > 1 then
		mrce_global_points = mrce_global_points - 250
	end

end

local function drainRings(p)
	if p.rings > 0 then
		-- p.rings = $ - p.acidpower
		-- p.acidpower = $ * 2
		p.timetoacid = 12
		p.rings = p.rings - 1
	elseif p.playerstate == PST_LIVE then
		P_KillMobj(p.mo)
	end
end


addHook("PlayerThink", function(p)
	if not (p and p.mo and p.mo.valid) then return end

	if p.powers[pw_invulnerability]
	or p.powers[pw_flashing]
	or (p.powers[pw_shield] & SH_PROTECTWATER)
	or gamemap ~= 128 then return end



	if P_PlayerTouchingSectorSpecialFlag(p, SSF_RETURNFLAG) then
		p.acidtrigger = true
	end
	
	
	if p.acidtrigger ~= nil then
		if p.acidtrigger then
			--print("I am dying")
			if p.timetoacid then
				p.timetoacid = $ - 1
			else
				drainRings(p)
				drainScore(p)
			end
			p.acidtrigger = false
		else
			p.timetoacid = 5
			p.acidpower = 1
			p.acidtrigger = nil
		end
	end
end)

addHook("LinedefExecute", function(line, mo, sector)
	if mo.player then
		mo.player.acidtrigger = true
	end
end, "ACIDNCZ")